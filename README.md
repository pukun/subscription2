## Plan

### Controller
1. Scheduler fulfills lifecycle management of replications;

### Replicator
1. metrics/prometheus/grafana

### Integration
1. k8s(service/db)

## Worker status in the controller

| status | description                                                                               |
| ---- |-------------------------------------------------------------------------------------------|
| FOLLOWING | Worker is following and can be assined tasks                                              |
| LEFT | Worker has left the group, no tasks will be assigned and messages from it will be ignored |
| JOINNING | Worker's been considered `LEFT` but the node is asking to apply again.                   |

## Message from node

### Join
### Follow

## Guarantee

### Worker side

#### After `Follow` message being rejected, workers should reset themselves(**PASSIVE RESET**)

#### When the controller can't be reached after some time, workers should reset themselves(**ACTIVE RESET**)

#### When workers are reset, all tasks assigned to them should be stopped and related messages

#### After being reset, all messages relating to tasks assigned before reset should be suppressed and not send to the controller

#### After reset, workers send `Join` message to attain the membership

#### Workers continue sending `Join` message until being accepted, then send `Follow` message instead

#### Consider performance and correctness, only one upstream connection should be established between node and controller.

### Controller side

#### When workers left the group, all task-related messages will be ignored

#### When

## Some conclusions

### From tasks' perspective, activation/Deactivation messages will only come from the assigned workers

#### Prove
**TBD**
