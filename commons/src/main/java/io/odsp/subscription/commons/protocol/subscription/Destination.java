package io.odsp.subscription.commons.protocol.subscription;

public record Destination(String path) {
    public static Destination of(String path) {
        return new Destination(path);
    }
}
