package io.odsp.subscription.commons.content;

import java.util.Optional;

public sealed interface Tailor permits XMLTailor, JSONTailor {
    String tailor(String content, String expression);

    Optional<String> validate(String expression);
}
