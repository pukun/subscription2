package io.odsp.subscription.commons.protocol.membership;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import static io.odsp.subscription.commons.protocol.membership.PingResponse.*;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "response"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Pong.class, name = "pong"),
        @JsonSubTypes.Type(value = NoMembership.class, name = "noMembership"),
        @JsonSubTypes.Type(value = Mismatch.class, name = "mismatch")
})
public sealed interface PingResponse permits Pong, NoMembership, Mismatch {
    static Pong pong() {
        return new Pong();
    }

    static NoMembership noMembership() {
        return new NoMembership();
    }

    static Mismatch mismatch() {
        return new Mismatch();
    }


    default boolean isOk() {
        return false;
    }


    record Pong() implements PingResponse {
        @Override
        public boolean isOk() {
            return true;
        }

        public String content() {
            return "pong";
        }
    }

    record NoMembership() implements PingResponse {
        public String content() {
            return "noMembership";
        }
    }

    record Mismatch() implements PingResponse {
        public String content() {
            return "mismatch";
        }
    }
}
