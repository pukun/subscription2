package io.odsp.subscription.commons.content;

public record TailorExpression(String exp) {
    public static TailorExpression of(String exp) {
        return new TailorExpression(exp);
    }
}
