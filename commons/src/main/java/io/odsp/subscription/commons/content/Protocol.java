package io.odsp.subscription.commons.content;

public enum Protocol {
    CEPH_FS,
    KAFKA
}
