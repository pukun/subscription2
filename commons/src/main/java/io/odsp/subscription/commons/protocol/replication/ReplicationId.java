package io.odsp.subscription.commons.protocol.replication;

public record ReplicationId(int id) {
    public static ReplicationId of(int i) {
        return new ReplicationId(i);
    }
}
