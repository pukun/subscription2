package io.odsp.subscription.commons.protocol.membership;

public record Epoch(int value) {
    public Epoch next() {
        return new Epoch(value + 1);
    }

    public static Epoch first() {
        return new Epoch(1);
    }

    public static Epoch of(int value) {
        return new Epoch(value);
    }
}

