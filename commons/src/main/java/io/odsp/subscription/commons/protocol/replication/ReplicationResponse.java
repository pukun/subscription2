package io.odsp.subscription.commons.protocol.replication;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "result"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ReplicationResponse.Rejected.class, name = "rejected"),
        @JsonSubTypes.Type(value = ReplicationResponse.Started.class, name = "started"),
        @JsonSubTypes.Type(value = ReplicationResponse.Stopped.class, name = "stopped")
})
public sealed interface ReplicationResponse permits
        ReplicationResponse.Rejected,
        ReplicationResponse.Started,
        ReplicationResponse.Stopped {

    static Rejected reject(ReplicationId replication, String code, String detail) {
        return new Rejected(replication, code, detail);
    }

    record Rejected(ReplicationId replication, String code, String detail) implements ReplicationResponse {
    }

    static Started start(ReplicationId replication) {
        return new Started(replication);
    }

    record Started(ReplicationId replication) implements ReplicationResponse {
    }

    static Stopped stop(ReplicationId replication) {
        return new Stopped(replication);
    }

    record Stopped(ReplicationId replication) implements ReplicationResponse {
    }
}
