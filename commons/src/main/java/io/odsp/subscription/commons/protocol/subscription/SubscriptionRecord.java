package io.odsp.subscription.commons.protocol.subscription;

import java.time.Instant;

public record SubscriptionRecord(int id, SubscriptionSpec spec, int version, Instant createdAt, Instant updatedAt) {

}
