package io.odsp.subscription.commons.protocol.replication;

public record Replication(
        ReplicationId id,
        Subscription subscription,
        int total,
        int from,
        int to
) {
    public static final int TOTAL_PARTITIONS = 100;

    public int total() {
        return TOTAL_PARTITIONS;
    }
}
