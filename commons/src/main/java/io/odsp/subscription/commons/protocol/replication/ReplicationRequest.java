package io.odsp.subscription.commons.protocol.replication;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "request"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ReplicationRequest.Start.class, name = "start"),
        @JsonSubTypes.Type(value = ReplicationRequest.Stop.class, name = "stop")
})
public sealed interface ReplicationRequest permits ReplicationRequest.Start, ReplicationRequest.Stop {

    static Start start(Replication replication) {
        return new Start(replication);
    }

    record Start(Replication replication) implements ReplicationRequest {
    }

    static Stop stop(ReplicationId bundleId) {
        return new Stop(bundleId);
    }

    record Stop(ReplicationId replicationId) implements ReplicationRequest {

    }
}