package io.odsp.subscription.commons.protocol.membership;

public record MemberId(Node node, Epoch epoch) {
}
