package io.odsp.subscription.commons.protocol.membership;

import lombok.*;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Builder(toBuilder = true)
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Node implements Serializable {
    private String host;

    private int port;

    public Node() {
    }

    public Node(String h, int p) {
        this.host = h;
        this.port = p;
    }
}
