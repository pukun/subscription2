package io.odsp.subscription.commons.protocol.subscription;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.content.TailorExpression;

public record SubscriptionSpec(
        String application,
        Protocol protocol,
        Format format,
        String encoding,
        Source source, NamingPattern filter, TailorExpression tailor, Destination destination
) {
}
