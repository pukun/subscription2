package io.odsp.subscription.commons.protocol;

public record Error(String code, String message) {
    public static Error of(String code, String message) {
        return new Error(code, message);
    }
}
