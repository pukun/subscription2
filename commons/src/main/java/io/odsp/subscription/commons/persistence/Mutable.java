package io.odsp.subscription.commons.persistence;

import lombok.Getter;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.time.Instant;

@MappedSuperclass
@Getter
public class Mutable extends Immutable{
    @Column(name = "last_modified_time")
    @LastModifiedDate
    private Instant lastModifiedTime;

    @Version
    private int version;
}
