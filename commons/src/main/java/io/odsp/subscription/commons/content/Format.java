package io.odsp.subscription.commons.content;

public enum Format {
    XML,
    JSON
}
