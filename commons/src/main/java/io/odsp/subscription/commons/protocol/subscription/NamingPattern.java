package io.odsp.subscription.commons.protocol.subscription;

public record NamingPattern(String pattern) {
    public static NamingPattern of(String s) {
        return new NamingPattern(s);
    }
}
