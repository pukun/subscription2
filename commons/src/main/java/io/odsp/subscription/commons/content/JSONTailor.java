package io.odsp.subscription.commons.content;

import com.jayway.jsonpath.*;
import io.odsp.subscription.commons.content.Tailor;

import java.util.List;
import java.util.Optional;

public final class JSONTailor implements Tailor {
    @Override
    public String tailor(String content, String expression) {
        try {
            return stringify(prune(build(content), expression));
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public Optional<String> validate(String expression) {
        try {
            compile(expression);
            return Optional.empty();
        } catch (Exception e) {
            return Optional.of(e.getMessage());
        }
    }

    private JsonPath compile(String tailor) {
        return JsonPath.compile(tailor);
    }

    DocumentContext build(String content) {
        return JsonPath.parse(content);
    }

    DocumentContext prune(DocumentContext json, String expression) {
        return json.delete(expression);
    }

    String stringify(ReadContext json) {
        return json.jsonString();
    }

    public List<String> select(String content, String tailor) {
        return JsonPath.using(Configuration.defaultConfiguration().addOptions(Option.ALWAYS_RETURN_LIST))
                .parse(content).read(tailor);
    }
}
