package io.odsp.subscription.commons.content;

import io.odsp.subscription.commons.content.Tailor;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Optional;

public final class XMLTailor implements Tailor {

    @Override
    public String tailor(String document, String expression) {
        return stringify(prune(document, compile(expression)));
    }

    @Override
    public Optional<String> validate(String expression) {
        try {
            compile(expression);
            return Optional.empty();
        } catch (Exception e) {
            return Optional.of(e.getMessage());
        }
    }

    XPathExpression compile(String expression) {
        try {
            return XPathFactory.newInstance().newXPath().compile(expression);
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    Document build(String document) {
        try {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder()
                    .parse(new InputSource(new StringReader(document)));
        } catch (IOException | ParserConfigurationException | SAXException e) {
            throw new RuntimeException(e);
        }
    }

    NodeList select(String content, String tailor) throws XPathExpressionException {
        return (NodeList) compile(tailor).evaluate(build(content), XPathConstants.NODESET);
    }

    Document prune(String document, XPathExpression tailor) {
        try {
            Document xml = build(document);
            NodeList nodesToBePruned = (NodeList) tailor.evaluate(xml, XPathConstants.NODESET);
            for (int i = nodesToBePruned.getLength() - 1; i >= 0; i--) {
                Node node = nodesToBePruned.item(i);
                if (node instanceof Attr attr) attr.getOwnerElement().removeAttribute(attr.getName());
                else node.getParentNode().removeChild(node);
            }

            return xml;
        } catch (XPathExpressionException e) {
            throw new RuntimeException(e);
        }
    }

    String stringify(Document document) {
        try {
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(writer));
            return writer.toString();
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }
    }
}
