package io.odsp.subscription.commons.protocol;

import io.odsp.subscription.commons.protocol.membership.Epoch;

public interface Endpoints {
    String MEMBER = "/v1/replication/member";

    String PING = "/{epoch}/ping";
    String REPLICATE = "/{epoch}/replicate";
    String CANCEL= "/{epoch}/cancel";
    String NODE = "/node";
    String EPOCH = "/epoch";

    static String ping(Epoch epoch) {
        return String.join("/", MEMBER, "" + epoch.value(), "ping");
    }

    static String replicate(Epoch epoch) {
        return String.join("/", MEMBER, "" + epoch.value(), "replicate");
    }

    static String cancel(Epoch epoch) {
        return String.join("/", MEMBER, "" + epoch.value(), "cancel");
    }

    static String node() {
        return String.join("", MEMBER, NODE);
    }

    static String epoch() {
        return String.join("", MEMBER, EPOCH);
    }


    String CLUSTER = "/v1/replication/cluster";

    String JOIN = "/join";

    static String join() {
        return String.join("/", CLUSTER, "join");
    }

    static String member() {
        return String.join("/", CLUSTER, "member");
    }
}
