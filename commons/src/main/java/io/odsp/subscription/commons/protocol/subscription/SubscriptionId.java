package io.odsp.subscription.commons.protocol.subscription;

public record SubscriptionId(int id) {
    public static SubscriptionId of(int id) {
        return new SubscriptionId(id);
    }
}
