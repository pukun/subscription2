package io.odsp.subscription.commons.protocol.replication;

import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionSpec;

import java.time.Instant;

public record Subscription(SubscriptionId id, SubscriptionSpec spec) {

}
