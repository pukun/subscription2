package io.odsp.subscription.commons.protocol.subscription;

public record Source(String path) {
    public static Source of(String path) {
        return  new Source(path);
    }
}
