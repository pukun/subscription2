package io.odsp.subscription.commons.protocol.replication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class ReplicationResponseJsonTest {
    @Test
    public void encodeAndDecodeTest() throws JsonProcessingException {
        ReplicationResponse accepted = ReplicationResponse.start(ReplicationId.of(100));
        ReplicationResponse rejected = ReplicationResponse.reject(ReplicationId.of(200), "6", "test");

        ObjectMapper mapper = new ObjectMapper();

        System.out.println(mapper.writeValueAsString(accepted));
        System.out.println(mapper.writeValueAsString(rejected));
        String json = mapper.writeValueAsString(new ReplicationResponse[]{accepted, rejected});
        System.out.println(json);
        List<ReplicationResponse> responseList = Arrays.asList(mapper.readValue(json, ReplicationResponse[].class));
        assertThat(responseList, contains(accepted, rejected));

    }
}
