package io.odsp.subscription.commons.content;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

public class JSONTailorTest {
    private final JSONTailor tailor = new JSONTailor();

    private final String content = """
              {
                  "firstName": "John",
                  "lastName": "Doe",
                  "age": 42,
                  "address":
                  {
                     "streetAddress": "400 Some Street",
                     "city": "Beverly Hills",
                     "state": "CA",
                     "zipcode": 90210
                  },
                  "phoneNumbers":
                  [
                     {
                        "type": "home",
                        "number": "310 555-1234"
                     },
                     {
                        "type": "fax",
                        "number": "310 555-4567"
                     }
                  ]
              }
            """;

    @Test
    @DisplayName("stringify json object")
    public void stringify() {
        String json = tailor.stringify(tailor.build(content));

        assertThat(json, containsString("phoneNumbers"));

//        System.out.println(json);
    }

    @Nested
    @DisplayName("prune json object")
    class PruneJson {
        @Test
        public void pruneObject() {
            String exp = "$.phoneNumbers[?(@.type == 'home')].number";

            assertThat(tailor.select(content, exp).size(), is(1));
            assertThat(tailor.select(content, "$..number").size(), is(2));

            String tailored = tailor.tailor(content, exp);

            assertThat(tailor.select(tailored, exp).size(), is(0));
            assertThat(tailor.select(tailored, "$..number").size(), is(1));
        }
    }
}
