package io.odsp.subscription.commons.content;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import javax.xml.xpath.XPathExpressionException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

public class XMLTailorTest {
    private final XMLTailor tailor = new XMLTailor();

    private final String content = """
            <?xml version="1.0" encoding="UTF-8"?>
                        
            <bookstore>
                        
            <book category="cooking">
              <title lang="en">Everyday Italian</title>
              <author>Giada De Laurentiis</author>
              <year>2005</year>
              <price>30.00</price>
            </book>
                        
            <book category="children">
              <title lang="en">Harry Potter</title>
              <author>J K. Rowling</author>
              <year>2005</year>
              <price>29.99</price>
            </book>
                        
            <book category="web">
              <title lang="en">XQuery Kick Start</title>
              <author>James McGovern</author>
              <author>Per Bothner</author>
              <author>Kurt Cagle</author>
              <author>James Linn</author>
              <author>Vaidyanathan Nagarajan</author>
              <year>2003</year>
              <price>49.99</price>
            </book>
                        
            <book category="web">
              <title lang="en">Learning XML</title>
              <author>Erik T. Ray</author>
              <year>2003</year>
              <price>39.95</price>
            </book>
                        
            </bookstore>
            """;

    @Test
    @DisplayName("stringify xml")
    public void stringify() {
        String xml = tailor.stringify(tailor.build(content));

        assertThat(xml, containsString("encoding"));
//        System.out.println("XML content after being stringifyed");
//        System.out.println(xml);
    }

    @Nested
    @DisplayName("prune xml")
    class PruneXML {
        @Test
        @DisplayName("prune price nodes")
        public void prunePriceNode() throws XPathExpressionException {
            String tailor = "//book/price";

            assertThat(XMLTailorTest.this.tailor.select(content, tailor).getLength(), is(4));

            String pruned = XMLTailorTest.this.tailor.tailor(content, tailor);

            assertThat(XMLTailorTest.this.tailor.select(pruned, tailor).getLength(), is(0));
        }

        @Test
        @DisplayName("prune category attribute when value is 'web'")
        public void pruneCategoryAttribute() throws XPathExpressionException {
            String tailorExp = "//book[@category='web']/@category";
            assertThat(tailor.select(content, tailorExp).getLength(), is(2));

            String tailored = tailor.tailor(content, tailorExp);

            assertThat(tailor.select(tailored, tailorExp).getLength(), is(0));
        }

        @Test
        @DisplayName("prune with multiple select expression")
        public void pruneWithMultipleExpressions() throws XPathExpressionException {
            String tailorExp = "//book[@category='web']/@category | //book/price";

            assertThat(tailor.select(content, "//book/price").getLength(), is(4));
            assertThat(tailor.select(content, "//book[@category='web']").getLength(), is(2));

            String pruned = tailor.tailor(content, tailorExp);

            assertThat(tailor.select(pruned, "//book/price").getLength(), is(0));
            assertThat(tailor.select(pruned, "//book[@category='web']").getLength(), is(0));

        }
    }
}
