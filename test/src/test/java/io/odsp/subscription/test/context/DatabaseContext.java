package io.odsp.subscription.test.context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.Container;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.utility.DockerImageName;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class DatabaseContext {
    private static final Logger logger = LoggerFactory.getLogger("database");

    final static String CONTROLLER_SQL_PATH = "../controller/src/main/resources/META-INF/changelog";

    final static String WORKER_SQL_PATH = "../worker/src/main/resources/META-INF/changelog";

    final static String SCENARIO_SQL_PATH = "sql";

    final static String CONTAINER_DB_PATH = "/docker-entrypoint-initdb.d";

    final static String POSTGRES_HOST = "postgres";
    final static String POSTGRES_USER = "subscriptionSpec";
    final static String POSTGRES_PASSWORD = "subscriptionSpec";
    final static String POSTGRES_DB = "subscriptionSpec";

    public final static String DATASOURCE = String.format(
            "jdbc:postgresql://%s/%s?user=%s&password=%s",
            POSTGRES_HOST,
            POSTGRES_DB,
            POSTGRES_USER,
            POSTGRES_PASSWORD
    );


    public static void initialize(Network network) throws IOException, InterruptedException {
        prepareSQLScripts();

        GenericContainer<?> database = startContainer(network);

        //verify table 't_member' has been created
        List<String> cmd = new ArrayList<>(Arrays.asList(String.format("psql -d %s -U %s -c \\x -c", POSTGRES_DB, POSTGRES_USER).split(" ")));
        cmd.add("select count(*) from t_member");
        Container.ExecResult query = database.execInContainer(cmd.toArray(new String[0]));
        logger.info("query result: {}", query);
        assertThat(query.getStdout(), containsString("count | 0"));

    }

    public static void clear() throws IOException {
        final Path scriptBase = Path.of(SCENARIO_SQL_PATH);
        if (!Files.exists(scriptBase)) return;

        logger.info("remove path(\"{}\") containing initialize db scripts", SCENARIO_SQL_PATH);

        for (Path script : Files.list(scriptBase).toList()) Files.delete(script);
        Files.deleteIfExists(scriptBase);
    }


    private static void prepareSQLScripts() throws IOException {
        logger.info("prepare path(\"{}\") for building image", SCENARIO_SQL_PATH);
        Files.createDirectory(Path.of(SCENARIO_SQL_PATH).toAbsolutePath());

        prepareSQLScripts(Path.of(CONTROLLER_SQL_PATH), "controller");
        prepareSQLScripts(Path.of(WORKER_SQL_PATH), "worker");
    }

    private static void prepareSQLScripts(Path source, String prefix) throws IOException {
        for (Path script : Files.list(source).toList()) {
            String name = script.getFileName().toString();
            if (name.endsWith("sql"))
                Files.copy(script, Path.of(SCENARIO_SQL_PATH, String.join("-", prefix, name)));
        }
    }

    private static GenericContainer<?> startContainer(Network network) {
        GenericContainer<?> database = new GenericContainer<>(DockerImageName.parse("postgres:14-alpine"))
                .withEnv("POSTGRES_USER", POSTGRES_USER)
                .withEnv("POSTGRES_PASSWORD", POSTGRES_PASSWORD)
                .withFileSystemBind(SCENARIO_SQL_PATH, CONTAINER_DB_PATH)
                .withNetwork(network)
                .withNetworkAliases(POSTGRES_HOST)
                .withExposedPorts(5432);
        database.start();

        return database;
    }
}
