package io.odsp.subscription.test.scenario;

import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import io.odsp.subscription.test.context.ControllerContext;
import io.odsp.subscription.test.context.DatabaseContext;
import io.odsp.subscription.test.context.WorkerContext;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.Network;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;

public class MembershipScenario {
    private static final Logger logger = LoggerFactory.getLogger("membership");


    private final static Network network = Network.newNetwork();


    private WorkerContext worker;

    private ControllerContext controller;


    @BeforeAll
    public static void startDatabase() throws IOException, InterruptedException {
        DatabaseContext.clear();
        DatabaseContext.initialize(network);
    }

    @AfterAll
    public static void stopDatabase() throws IOException {
        DatabaseContext.clear();
    }

    @Test
    @DisplayName("membership changes based on node/network status")
    public void scenario() throws IOException, InterruptedException {
        membership_could_be_established_when_worker_and_controller_are_connected();
        worker_will_reset_when_controller_is_stopped();
        worker_can_rejoin_when_controller_recovers();
        controller_could_detect_worker_lost();
    }


    private void membership_could_be_established_when_worker_and_controller_are_connected() {
        worker = WorkerContext.start(network);
        controller = ControllerContext.start(network);

        Flux.interval(Duration.ofSeconds(1))
                .flatMap(i -> worker.asyncPing())
                .doOnEach(response -> logger.info("response of ping (when waiting for membership) {}", response.get()))
                .filter(PingResponse::isOk)
                .next().block();

        Node workerNode = worker.node();

        List<MemberId> members = controller.members();
        assertThat(members, contains(new MemberId(workerNode, Epoch.first())));
    }

    private void worker_will_reset_when_controller_is_stopped() {
        controller.stop();
        logger.info("controller has stopped");

        Flux.interval(Duration.ofSeconds(1))
                .flatMap(i -> worker.asyncEpoch())
                .doOnEach(response -> logger.info("current epoch (when controller stopped) {}", response.get()))
                .filter(Optional::isEmpty)
                .next().block();
        logger.info("worker has been reset");
    }

    private void worker_can_rejoin_when_controller_recovers() {
        logger.info("try to recover controller ...");
        controller = ControllerContext.start(network);
        logger.info("controller is recovered");

        Epoch epoch = Epoch.first().next();

        Flux.interval(Duration.ofSeconds(1))
                .flatMap(i -> worker.asyncPing(epoch))
                .doOnEach(response -> logger.info("response of ping (when controller recovered) {}", response.get()))
                .filter(PingResponse::isOk)
                .next().block();

        Node workerNode = worker.node();

        List<MemberId> members = controller.members();
        assertThat(members, contains(new MemberId(workerNode, epoch)));
        logger.info("Worker joined the cluster again");
    }

    private void controller_could_detect_worker_lost() {
        Node workerNode = worker.node();
        worker.stop();

        Flux.interval(Duration.ofSeconds(1))
                .flatMap(i -> controller.asyncMember(workerNode))
                .doOnEach(response -> logger.info("member id of worker(when the node is stopped) {}", response.get()))
                .filter(Optional::isEmpty)
                .next().block();

        assertThat(controller.members(), empty());
        logger.info("Worker failure is detected");
    }
}
