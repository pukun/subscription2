package io.odsp.subscription.test.context;

import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.utility.DockerImageName;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import java.util.List;
import java.util.Optional;

public record ControllerContext(GenericContainer<?> controller, WebClient client) {
    private static final Logger logger = LoggerFactory.getLogger("controller");

    public final static String CONTROLLER_HOST = "controller";
    public final static int CONTROLLER_PORT = 6159;

    public static ControllerContext start(Network network) {
        GenericContainer<?> controller = new GenericContainer<>(DockerImageName.parse("io.odsp/subscription2/controller"))
                .withEnv("spring.datasource.url", DatabaseContext.DATASOURCE)
                .withEnv("server.port", "" + CONTROLLER_PORT)
                .withNetwork(network)
                .withNetworkAliases(CONTROLLER_HOST)
                .withExposedPorts(CONTROLLER_PORT)
                .withLogConsumer(new Slf4jLogConsumer(logger));
        controller.start();

        String baseUrl = String.format("http://%s:%d", controller.getHost(), controller.getMappedPort(CONTROLLER_PORT));
        WebClient client = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(HttpClient.create()))
                .baseUrl(baseUrl)
                .build();

        return new ControllerContext(controller, client);
    }

    public List<MemberId> members() {
        return asyncMembers()
                .collectList().block();

    }

    public Flux<MemberId> asyncMembers() {
        return client.get().uri(Endpoints.CLUSTER)
                .retrieve()
                .bodyToFlux(MemberId.class);
    }

    public Mono<Optional<MemberId>> asyncMember(Node node) {
        return client.post().uri(Endpoints.member())
                .bodyValue(node)
                .retrieve()
                .bodyToMono(MemberId.class)
                .map(Optional::ofNullable)
                .onErrorResume(t -> Mono.just(Optional.empty()));
    }

    public void stop() {
        controller.stop();
    }
}
