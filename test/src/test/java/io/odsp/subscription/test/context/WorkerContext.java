package io.odsp.subscription.test.context;

import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.utility.DockerImageName;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public record WorkerContext(GenericContainer<?> worker, WebClient client) {
    private static final Logger logger = LoggerFactory.getLogger("worker");

    public final static String WORKER_HOST = "worker";
    public final static int WORKER_PORT = 6160;

    public static WorkerContext start(Network network) {
        GenericContainer<?> worker = new GenericContainer<>(DockerImageName.parse("io.odsp/subscription2/worker"))
                .withEnv("spring.datasource.url", DatabaseContext.DATASOURCE)
                .withEnv("server.port", "" + WORKER_PORT)
                .withEnv("worker.membership.cluster.host", ControllerContext.CONTROLLER_HOST)
                .withEnv("worker.membership.cluster.port", "" + ControllerContext.CONTROLLER_PORT)
                .withEnv("worker.membership.apply.interval", "" + 5)
                .withEnv("worker.membership.failure.threshold", "" + 10)
                .withNetwork(network)
                .withNetworkAliases(WORKER_HOST)
                .withExposedPorts(WORKER_PORT)
                .withLogConsumer(new Slf4jLogConsumer(logger));
        worker.start();

        String baseUrl = String.format("http://%s:%d", worker.getHost(), worker.getMappedPort(WORKER_PORT));
        WebClient client = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(HttpClient.create()))
                .baseUrl(baseUrl)
                .build();

        WorkerContext context = new WorkerContext(worker, client);
        assertThat(context.ping(), equalTo(PingResponse.noMembership()));

        return context;
    }

    public Node node() {
        return client.get().uri(Endpoints.node())
                .retrieve()
                .bodyToMono(Node.class)
                .block();
    }

    public PingResponse ping() {
        return ping(Epoch.first());
    }

    public PingResponse ping(Epoch epoch) {
        return asyncPing(epoch)
                .block();
    }

    public Mono<PingResponse> asyncPing() {
        return asyncPing(Epoch.first());
    }

    public Mono<PingResponse> asyncPing(Epoch epoch) {
        return client.post().uri(Endpoints.ping(epoch))
                .retrieve()
                .bodyToMono(PingResponse.class);
    }

    public Mono<Optional<Epoch>> asyncEpoch() {
        return client.get().uri(Endpoints.epoch())
                .retrieve()
                .bodyToMono(Epoch.class)
                .map(Optional::ofNullable)
                .onErrorResume(t -> Mono.just(Optional.empty()));

    }

    public void stop() {
        worker.stop();
    }
}
