CREATE SEQUENCE seq_replication INCREMENT BY 100 MINVALUE 1 MAXVALUE 999999999 START WITH 1;
CREATE SEQUENCE seq_subscription INCREMENT BY 1 MINVALUE 1 MAXVALUE 99999999 START WITH 1;
CREATE TABLE t_replication_bundle (
  id                 int4 NOT NULL, 
  subscription_id    int4 NOT NULL, 
  sequence           int2, 
  "size"             int2, 
  status             varchar(16) NOT NULL, 
  member_host        varchar(255), 
  member_port        int2, 
  member_epoch       int2, 
  created_time       timestamp NOT NULL, 
  last_modified_time timestamp NOT NULL, 
  version            int4, 
  PRIMARY KEY (id));
CREATE TABLE t_subscription (
  id                 int4 NOT NULL, 
  application        varchar(64) NOT NULL, 
  protocol           varchar(16) NOT NULL, 
  format             varchar(16) NOT NULL, 
  encoding           varchar(16) NOT NULL, 
  source             varchar(255) NOT NULL, 
  filter             varchar(255), 
  tailor             varchar(255), 
  destination        varchar(255) NOT NULL, 
  active             bool DEFAULT 'false' NOT NULL, 
  activated_time     timestamp NOT NULL, 
  deactivated_time   timestamp, 
  created_time       timestamp NOT NULL, 
  last_modified_time timestamp NOT NULL, 
  version            int4 NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE t_member (
  host               varchar(255) NOT NULL, 
  port               int2 NOT NULL, 
  epoch              int4 NOT NULL, 
  status             varchar(16) NOT NULL, 
  created_time       timestamp NOT NULL, 
  last_modified_time timestamp NOT NULL, 
  version            int4 NOT NULL, 
  PRIMARY KEY (host, 
  port));
CREATE INDEX idx_replication_group_subscription 
  ON t_replication_bundle (subscription_id);
CREATE UNIQUE INDEX t_subscription_app_idx 
  ON t_subscription (application, protocol, format, encoding, source) WHERE active;
