package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.Node;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Node> {
    @Query("select m from Member m where m.status = 'UP'")
    List<Member> getAllActiveMembers();


}
