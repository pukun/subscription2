package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

record ClusterContext(
        MemberChannelFactory channelFactory,
        MemberRepository repository,
        TransactionTemplate tx,
        Sinks.Many<ClusterChange> source,
        FailureDetector failureDetector
) {

    //member creation
    List<Member> loadAllActiveMembers() {
        return repository.getAllActiveMembers().stream()
                .map(m -> m.channel(channelOf(m.id())))
                .collect(Collectors.toList());
    }

    Member create(Node node) {
        return save(Member.of(node, Epoch.first(), channelFactory));
    }

    Member age(MemberId memberId) {
        return save(Member.age(memberId.node(), memberId.epoch(), channelFactory));
    }

    Optional<Member> historyMemberOf(Node node) {
        return repository.findById(node).map(m -> m.channel(channelOf(m.id())));
    }

    Member leave(Member member) {
        return save(member.leave());
    }

    Member save(Member member) {
        return tx.execute(s -> repository.save(member));
    }

    private MemberChannel channelOf(MemberId memberId) {
        return channelFactory.channelOf(memberId);
    }
    //endregion

    //region cluster change
    Flux<ClusterChange> fluxOfChange() {
        return source.asFlux();
    }

    void emitChangeOfAgingMember(MemberId member, Epoch agedMember) {
        source.tryEmitNext(ClusterChange.memberAged(member, agedMember));
    }

    void emitChangeOfNewMember(Member member) {
        source.tryEmitNext(ClusterChange.memberJoined(member.id()));
    }

    void emitChangeOfDispersedMember(Member member) {
        source.tryEmitNext(ClusterChange.memberLeft(member.id()));
    }
    //endregion

    Mono<Boolean> watch(Member member) {
        return failureDetector.watch(member);
    }
}
