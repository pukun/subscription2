package io.odsp.subscription.controller.subscription.validator;

import io.odsp.subscription.commons.content.Protocol;

public class AccessibilityValidators {
    private static final CephFSValidator cephConnector = new CephFSValidator();

    public static AccessibilityValidationResult check(Protocol protocol, String url) {
        if (protocol == Protocol.CEPH_FS) return cephConnector.test(url);
        else return new AccessibilityValidationResult(true, false, false);
    }
}
