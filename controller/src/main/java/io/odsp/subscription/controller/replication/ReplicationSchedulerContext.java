package io.odsp.subscription.controller.replication;

import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import io.odsp.subscription.commons.protocol.replication.ReplicationResponse;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.controller.member.ClusterChange;
import io.odsp.subscription.controller.member.ClusterChangeSource;
import io.odsp.subscription.controller.member.ReplicationProxy;
import io.odsp.subscription.controller.subscription.Subscription;
import io.odsp.subscription.controller.subscription.SubscriptionChange;
import io.odsp.subscription.controller.subscription.SubscriptionChangeSource;
import io.odsp.subscription.controller.subscription.SubscriptionRepository;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

public record ReplicationSchedulerContext(
        ClusterChangeSource clusterChangeSource,
        ReplicationProxy replicationProxy,
        SubscriptionChangeSource subscriptionChangeSource,
        TransactionTemplate txTemplate,
        ReplicationBundleRepository repository,
        SubscriptionRepository subscriptionRepository,
        Duration subscriptionScreenInterval,
        Duration replicationScreenInterval,
        int nodeCapacityLimit
) {
    ReplicationSchedule emptySchedule() {
        return ReplicationSchedule.empty(nodeCapacityLimit);
    }

    Flux<SubscriptionChange> subscriptionChange() {
        return subscriptionChangeSource.observe();
    }

    Flux<ClusterChange> clusterChange() {
        return clusterChangeSource.observe();
    }

    List<SubscriptionChange.SubscriptionActivation> strandedActiveSubscriptions(Instant checkMoment) {
        return repository.findActiveSubscriptionsHaveNotBeenStarted(checkMoment)
                .stream().map(SubscriptionId::of)
                .map(SubscriptionChange::subscribed)
                .toList();
    }

    List<SubscriptionChange.SubscriptionDeactivation> strandedInActiveSubscriptions(Instant checkMoment) {
        return repository.findInActiveSubscriptionsHaveNotBeenStopped(checkMoment)
                .stream().map(SubscriptionId::of)
                .map(SubscriptionChange::unsubscribed)
                .toList();
    }

    Optional<Subscription> subscriptionOf(SubscriptionId id) {
        return subscriptionRepository.findById(id.id());
    }

    Mono<List<ReplicationResponse>> invoke(MemberId member, List<ReplicationRequest> requests) {
        return replicationProxy.replicate(member, requests).collectList();
    }

    void save(List<ReplicationChange> changes) {
        txTemplate.executeWithoutResult(s -> changes.forEach(change -> repository.save(change.bundle())));
    }
}
