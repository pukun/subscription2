package io.odsp.subscription.controller.subscription.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.nio.charset.Charset;

@Converter
public class EncodingConverter implements AttributeConverter<Charset, String> {
    @Override
    public String convertToDatabaseColumn(Charset charset) {
        return charset.displayName();
    }

    @Override
    public Charset convertToEntityAttribute(String charset) {
        return Charset.forName(charset);
    }
}
