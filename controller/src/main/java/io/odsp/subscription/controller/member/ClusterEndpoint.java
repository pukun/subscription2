package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.Error;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Optional;

import static org.springframework.http.ResponseEntity.internalServerError;

@RestController
@RequestMapping(Endpoints.CLUSTER)
public class ClusterEndpoint {
    private final static Logger logger = LoggerFactory.getLogger("cluster");

    @Autowired
    private Cluster cluster;

    @PostMapping(Endpoints.JOIN)
    public Mono<ResponseEntity<Object>> join(@RequestBody Node node) {
        logger.info("Receive join request from {} ", node);

        return cluster.onJoinRequest(node)
                .map(this::joinSucceeded)
                .onErrorResume(t -> Mono.just(joinFailed(t)))
                .doOnSuccess(r -> logger.info("worker joined successfully with id {}", r.getBody()))
                .doOnError(t -> logger.info("Failed to process join request from {}", node, t));
    }

    @GetMapping
    public Flux<MemberId> members() {
        return Flux.fromIterable(cluster.members());
    }

    @PostMapping("/member")
    public Mono<MemberId> member(@RequestBody Node node) {
        Optional<MemberId> member = cluster.memberOf(node);
        if (member.isPresent()) return Mono.just(member.get());
        else throw new MemberNotInCluster();
    }

    private ResponseEntity<Object> joinSucceeded(MemberId memberId) {
        return ResponseEntity.ok(memberId);
    }

    private ResponseEntity<Object> joinFailed(Throwable t) {
        return internalServerError().body(Error.of("UnexpectedError", t.getMessage()));
    }

    static class MemberNotInCluster extends RuntimeException {
    }

    @ExceptionHandler
    public ResponseEntity<Error> onError(MemberNotInCluster t) {
        return ResponseEntity.notFound().build();
    }

}
