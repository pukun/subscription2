package io.odsp.subscription.controller.replication;

import io.odsp.subscription.commons.protocol.replication.Replication;
import io.odsp.subscription.commons.protocol.replication.ReplicationId;
import io.odsp.subscription.controller.subscription.SubscriptionMapper;
import org.apache.commons.lang3.tuple.Pair;

public class ReplicationMapper {
    public static Replication replicationOf(ReplicationBundle bundle) {
        Pair<Integer, Integer> range = rangeOf(bundle);
        return new Replication(
                ReplicationId.of(bundle.id()),
                SubscriptionMapper.baseOf(bundle.subscription()),
                Replication.TOTAL_PARTITIONS,
                range.getLeft(), range.getRight()
        );
    }

    static Pair<Integer, Integer> rangeOf(ReplicationBundle bundle) {
        int from = bundle.sequence() * bundle.size();
        int to = from + bundle.size() - 1;
        if (to >= Replication.TOTAL_PARTITIONS) to = Replication.TOTAL_PARTITIONS - 1;

        return Pair.of(from, to);
    }
}
