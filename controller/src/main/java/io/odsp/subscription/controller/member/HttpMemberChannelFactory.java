package io.odsp.subscription.controller.member;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;


public record HttpMemberChannelFactory(int timeout) implements MemberChannelFactory {

    @Override
    public MemberChannel channelOf(MemberId memberId) {
        return channelOf(memberId, urlOf(memberId));
    }

    HttpMemberChannel channelOf(MemberId memberId, String baseUrl) {
        HttpClient httpClient = HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, timeout * 1000)
                .doOnConnected(conn -> conn
                        .addHandlerLast(new ReadTimeoutHandler(timeout))
                        .addHandlerLast(new WriteTimeoutHandler(timeout)));

        WebClient webClient = WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .baseUrl(baseUrl)
                .build();

        return new HttpMemberChannel(memberId, webClient);
    }

    String urlOf(MemberId memberId) {
        return String.format("http://%s:%s/", memberId.node().getHost(), memberId.node().getPort());
    }
}
