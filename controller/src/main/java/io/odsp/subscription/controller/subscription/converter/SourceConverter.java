package io.odsp.subscription.controller.subscription.converter;

import io.odsp.subscription.commons.protocol.subscription.Source;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class SourceConverter implements AttributeConverter<Source, String> {
    @Override
    public String convertToDatabaseColumn(Source attribute) {
        return attribute.path();
    }

    @Override
    public Source convertToEntityAttribute(String dbData) {
        return Source.of(dbData);
    }
}
