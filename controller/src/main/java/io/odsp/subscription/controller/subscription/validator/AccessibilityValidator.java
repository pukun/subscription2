package io.odsp.subscription.controller.subscription.validator;

import io.odsp.subscription.commons.content.Protocol;

public sealed interface AccessibilityValidator permits CephFSValidator {
    Protocol protocol();

    AccessibilityValidationResult test(String url);
}
