package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.Epoch;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class EpochConverter implements AttributeConverter<Epoch, Integer> {
    @Override
    public Integer convertToDatabaseColumn(Epoch epoch) {
        return epoch == null ? null :epoch.value();
    }

    @Override
    public Epoch convertToEntityAttribute(Integer memberId) {
        return new Epoch(memberId);
    }
}
