package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.commons.protocol.subscription.SubscriptionRecord;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionSpec;

public class SubscriptionMapper {
    public static SubscriptionRecord recordOf(Subscription subscription) {
        return new SubscriptionRecord(subscription.sid().id(), specOf(subscription), subscription.getVersion(), subscription.getCreatedTime(), subscription.getLastModifiedTime());
    }

    public static SubscriptionSpec specOf(Subscription subscription) {
        return new SubscriptionSpec(
                subscription.application(),
                subscription.protocol(),
                subscription.format(),
                subscription.encoding().name(),
                subscription.source(),
                subscription.filter(),
                subscription.tailor(),
                subscription.destination()
        );
    }

    public static io.odsp.subscription.commons.protocol.replication.Subscription baseOf(Subscription subscription) {
        return new io.odsp.subscription.commons.protocol.replication.Subscription(subscription.sid(), specOf(subscription));
    }
}
