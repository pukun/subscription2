package io.odsp.subscription.controller.replication;

import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.replication.ReplicationId;
import io.odsp.subscription.commons.protocol.replication.ReplicationResponse;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.controller.subscription.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

record ReplicationSchedule(
        Set<SubscriptionId> subscriptions,
        Map<ReplicationId, ReplicationBundle> bundles,
        Map<MemberId, Integer> members,
        int concurrentBoundPerNode
) {
    private static final Logger logger = LoggerFactory.getLogger("replication");

    static ReplicationSchedule empty(int nodeCapacityLimit) {
        return new ReplicationSchedule(new HashSet<>(), new HashMap<>(), new HashMap<>(), nodeCapacityLimit);
    }

    List<ReplicationChange> onSubscribed(ReplicationBundleFactory factory, Subscription subscription) {
        if (!subscription.active()) return Collections.emptyList();

        if (subscriptions.contains(subscription.sid())) return Collections.emptyList();

        List<ReplicationBundle> created = factory.replicationsFor(subscription);
        created.forEach(bundle -> bundles.put(bundle.rid(), bundle));
        subscriptions.add(subscription.sid());

        List<ReplicationChange> changes = new ArrayList<>();
        List<ReplicationChange.Scheduled> scheduled = schedule(created);

        changes.addAll(scheduled);
        changes.addAll(created.subList(scheduled.size(), created.size()).stream()
                .map(ReplicationChange.Suspended::new)
                .toList()
        );

        return changes;
    }

    List<ReplicationChange> onUnsubscribed(Subscription subscription) {
        if (!subscription.active()) return Collections.emptyList();

        if (!subscriptions.contains(subscription.sid())) return Collections.emptyList();

        return bundles.values().stream()
                .filter(b -> b.subscription().id() == subscription.id())
                .map(this::onDismiss)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
    }

    private Optional<ReplicationChange> onDismiss(ReplicationBundle bundle) {
        if (!bundle.onDismissed()) return Optional.empty();

        if (bundle.isActive()) return Optional.of(new ReplicationChange.Dismissed(bundle));
        else return Optional.of(new ReplicationChange.Stopped(bundle));
    }

    List<ReplicationChange.Scheduled> schedule(List<ReplicationBundle> pendingBundles) {
        int scheduled = 0;
        for (MemberId m : members.keySet()) {
            int dispatched = members.get(m);
            int available = concurrentBoundPerNode - dispatched;
            if (available <= 0) continue;

            int scheduling = scheduled + available;
            if (scheduling > pendingBundles.size()) scheduling = pendingBundles.size();

            pendingBundles.subList(scheduled, scheduling).forEach(bundle -> bundle.onScheduled(m));
            members.put(m, dispatched + scheduling - scheduled);

            scheduled = scheduling;
            if (scheduled == pendingBundles.size()) break;
        }

        return pendingBundles.subList(0, scheduled).stream()
                .map(ReplicationChange.Scheduled::new)
                .toList();
    }

    List<ReplicationChange> onResponses(List<ReplicationResponse> responses) {
        return responses.stream()
                .map(this::onResponse)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
    }

    private Optional<ReplicationChange> onResponse(ReplicationResponse response) {
        if (response instanceof ReplicationResponse.Started started)
            return Optional.ofNullable(bundles.get(started.replication()))
                    .filter(ReplicationBundle::onStarted)
                    .map(ReplicationChange.Started::new);
        else if (response instanceof ReplicationResponse.Stopped stopped)
            return Optional.ofNullable(bundles.get(stopped.replication()))
                    .filter(ReplicationBundle::onStopped)
                    .map(ReplicationChange.Stopped::new);
        else return Optional.empty();
    }
}
