package io.odsp.subscription.controller.replication;


import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.replication.ReplicationId;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Optional;

import static io.odsp.subscription.commons.protocol.replication.ReplicationRequest.start;
import static io.odsp.subscription.commons.protocol.replication.ReplicationRequest.stop;

sealed interface ReplicationChange permits ReplicationChange.Suspended, ReplicationChange.Scheduled, ReplicationChange.Started, ReplicationChange.Dismissed, ReplicationChange.Stopped {
    ReplicationBundle bundle();

    default Optional<Pair<MemberId, ReplicationRequest>> scheduleOf() {
        return Optional.empty();
    }

    record Suspended(ReplicationBundle bundle) implements ReplicationChange {

    }

    record Scheduled(ReplicationBundle bundle) implements ReplicationChange {
        @Override
        public Optional<Pair<MemberId, ReplicationRequest>> scheduleOf() {
            return bundle.placement().map(m -> Pair.of(m, start(ReplicationMapper.replicationOf(bundle))));
        }
    }

    record Started(ReplicationBundle bundle) implements ReplicationChange {

    }

    record Dismissed(ReplicationBundle bundle) implements ReplicationChange {
        @Override
        public Optional<Pair<MemberId, ReplicationRequest>> scheduleOf() {
            return bundle.placement().map(m -> Pair.of(m, stop(ReplicationId.of(bundle.id()))));
        }
    }

    record Stopped(ReplicationBundle bundle) implements ReplicationChange {

    }
}
