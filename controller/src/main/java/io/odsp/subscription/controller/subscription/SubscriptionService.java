package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.Optional;

import static io.odsp.subscription.controller.subscription.SubscriptionChange.subscribed;
import static io.odsp.subscription.controller.subscription.SubscriptionChange.unsubscribed;
import static io.odsp.subscription.controller.subscription.SubscriptionResult.Subscribed;
import static io.odsp.subscription.controller.subscription.SubscriptionResult.unknownException;

@Service
public class SubscriptionService implements SubscriptionChangeSource {
    private static final Logger logger = LoggerFactory.getLogger(SubscriptionService.class);


    private final SubscriptionRepository repository;

    private final TransactionTemplate txTemplate;

    private final SubscriptionFactory factory;

    private final Sinks.Many<SubscriptionChange> channel;

    public SubscriptionService(SubscriptionRepository repository, TransactionTemplate transaction) {
        this.repository = repository;
        this.txTemplate = transaction;

        this.factory = new SubscriptionFactory(repository);

        this.channel = Sinks.many().unicast().onBackpressureError();
    }

    @Override
    public Flux<SubscriptionChange> observe() {
        return channel.asFlux();
    }

    public SubscriptionResult subscribe(SubscriptionSpec spec) {
        SubscriptionResult subscriptionResult = activateSubscription(spec);

        if (subscriptionResult instanceof Subscribed subscribed) {
            SubscriptionId subscriptionId = subscribed.subscription().sid();
            channel.tryEmitNext(subscribed(subscriptionId));
        }

        return subscriptionResult;
    }

    public Optional<Subscription> unsubscribe(SubscriptionId id) {
        Optional<Subscription> subscription = unsubscribeIfActive(id);

        subscription.ifPresent(s -> channel.tryEmitNext(unsubscribed(id)));

        return subscription.or(() -> repository.findById(id.id()));
    }

    private SubscriptionResult activateSubscription(SubscriptionSpec subscription) {
        SubscriptionResult subscriptionResult = factory.create(subscription);

        if (!(subscriptionResult instanceof Subscribed subscribed)) return subscriptionResult;

        txTemplate.executeWithoutResult(action -> repository.putIfAbsent(subscribed.subscription()));
        return repository.findActiveSubscription(subscribed.subscription())
                .map(SubscriptionResult::subscribed)
                .orElseGet(() -> unknownException("can't find activated subscriptionSpec in repository"));
    }

    private Optional<Subscription> unsubscribeIfActive(SubscriptionId id) {
        return txTemplate.execute(
                action -> repository.findActiveSubscriptionById(id)
                        .map(subscription -> repository.save(subscription.unsubscribe()))
        );
    }

}
