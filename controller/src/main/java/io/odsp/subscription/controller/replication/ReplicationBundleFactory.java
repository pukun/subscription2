package io.odsp.subscription.controller.replication;

import io.odsp.subscription.commons.protocol.replication.Replication;
import io.odsp.subscription.controller.subscription.Subscription;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ReplicationBundleFactory {
    public static final int DEFAULT_BUNDLE_SIZE = 10;

    private final int defaultBundleSize;

    public ReplicationBundleFactory() {
        defaultBundleSize = DEFAULT_BUNDLE_SIZE;
    }

    ReplicationBundleFactory(int defaultBundleSize) {
        this.defaultBundleSize = defaultBundleSize;
    }

    public List<ReplicationBundle> replicationsFor(Subscription subscription) {
        int size = sizeOfBundle(subscription);

        return IntStream.range(0, numberOBundles(size))
                .mapToObj(sequence -> replicationsFor(subscription, sequence, size))
                .collect(Collectors.toList());
    }

    private ReplicationBundle replicationsFor(Subscription subscription, int bundleSequence, int bundleSize) {
        return new ReplicationBundle()
                .subscription(subscription)
                .sequence(bundleSequence)
                .size(bundleSize)
                .status(ReplicationBundleStatus.SCHEDULING);
    }

    private int sizeOfBundle(Subscription subscription) {
        return defaultBundleSize;
    }

    private int numberOBundles(int bundleSize) {
        int bundles = Replication.TOTAL_PARTITIONS / bundleSize;
        return Replication.TOTAL_PARTITIONS % bundleSize == 0 ? bundles : bundles + 1;
    }
}
