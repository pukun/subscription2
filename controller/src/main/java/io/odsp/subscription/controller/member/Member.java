package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.persistence.Mutable;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import io.odsp.subscription.commons.protocol.replication.ReplicationResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "t_member")
@Accessors(fluent = true, chain = true)
@Getter
@Setter
public class Member extends Mutable {

    @EmbeddedId
    private Node node;

    @Convert(converter = EpochConverter.class)
    private volatile Epoch epoch;

    @Enumerated(EnumType.STRING)
    private MemberStatus status;

    @Transient
    private MemberChannel channel;

    public Member() {

    }

    public static Member of(Node node, Epoch epoch, MemberChannelFactory channelFactory) {
        Member member = new Member();
        member.node = node;
        member.epoch = epoch;
        member.status = MemberStatus.UP;
        member.channel = channelFactory.channelOf(member.id());

        return member;
    }

    public static Member age(Node node, Epoch epoch, MemberChannelFactory channelFactory) {
        return Member.of(node, epoch.next(), channelFactory);
    }

    //region invocation
    public Flux<ReplicationResponse> replicate(List<ReplicationRequest> requests) {
        return channel.replicate(requests);
    }
    //endregion

    //region membership

    Member leave() {
        if (!isUp()) return this;

        status = MemberStatus.DOWN;

        return this;
    }

    Mono<PingResponse> ping() {
        if (!isUp()) return Mono.error(new RuntimeException("Member not in the cluster can't be pinged"));
        else return channel.ping();
    }

    public MemberId id() {
        return new MemberId(node, epoch);
    }

    public boolean isUp() {
        return MemberStatus.UP == status;
    }
    //endregion

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("worker", node)
                .append("epoch", epoch)
                .append("status", status)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member member)) return false;
        return node.equals(member.node) && epoch.equals(member.epoch);
    }

    @Override
    public int hashCode() {
        return Objects.hash(node, epoch);
    }

}
