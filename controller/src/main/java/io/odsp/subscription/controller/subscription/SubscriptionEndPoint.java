package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.commons.protocol.Error;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionRecord;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionSpec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/v1/subscriptions")
public class SubscriptionEndPoint {
    private final static Logger logger = LoggerFactory.getLogger(SubscriptionEndPoint.class);

    @Autowired
    private SubscriptionService service;


    @PostMapping
    public Mono<ResponseEntity<?>> subscribe(@RequestBody SubscriptionSpec request) {
        return Mono.fromCallable(() -> service.subscribe(request))
                .map(this::onSubscribed);
    }

    @PostMapping("/{subscriptionSpec}/unsubscribe")
    public Mono<ResponseEntity<?>> unsubscribe(@PathVariable("subscriptionSpec") int subscription) {
        return Mono.fromCallable(() -> service.unsubscribe(SubscriptionId.of(subscription)))
                .map(termination -> termination.isEmpty() ?
                        ResponseEntity.notFound().build() :
                        ResponseEntity.ok(SubscriptionMapper.recordOf(termination.get()))
                );
    }

    private ResponseEntity<?> onSubscribed(SubscriptionResult result) {
        if (result instanceof SubscriptionResult.Subscribed subscribed) return ResponseEntity.status(HttpStatus.CREATED)
                .body(SubscriptionMapper.recordOf(subscribed.subscription()));
        else if (result instanceof SubscriptionResult.IllegalSpec illegalSpec)
            return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                    .body(new Error("IllegalSpec", illegalSpec.detail()));
        else if (result instanceof SubscriptionResult.UnknownException exception)
            return ResponseEntity.internalServerError()
                    .body(new Error("UnexpectedError", exception.detail()));

        return null;
    }



}
