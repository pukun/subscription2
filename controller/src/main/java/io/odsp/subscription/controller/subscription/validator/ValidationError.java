package io.odsp.subscription.controller.subscription.validator;

public enum ValidationError {
    InaccessibleDestination, InaccessibleSource, InvalidTailor,
    UnknownProtocol, UnknownFormat, UnknownEncoding
}
