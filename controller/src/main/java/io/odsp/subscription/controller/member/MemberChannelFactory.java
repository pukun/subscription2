package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.MemberId;

public interface MemberChannelFactory {
    MemberChannel channelOf(MemberId memberId);
}
