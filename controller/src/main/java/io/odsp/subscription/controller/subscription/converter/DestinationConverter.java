package io.odsp.subscription.controller.subscription.converter;

import io.odsp.subscription.commons.protocol.subscription.Destination;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class DestinationConverter implements AttributeConverter<Destination, String> {
    @Override
    public String convertToDatabaseColumn(Destination attribute) {
        return attribute.path();
    }

    @Override
    public Destination convertToEntityAttribute(String dbData) {
        return Destination.of(dbData);
    }
}
