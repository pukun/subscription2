package io.odsp.subscription.controller.subscription.validator;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.JSONTailor;
import io.odsp.subscription.commons.content.Tailor;
import io.odsp.subscription.commons.content.XMLTailor;

public class TailorValidator {
    final static JSONTailor jsonTailor = new JSONTailor();
    final static XMLTailor xmlTailor = new XMLTailor();

    public static TailorValidationResult test(Format format, String exp) {
        Tailor tailor = format == Format.JSON ? jsonTailor : xmlTailor;

        return tailor.validate(exp).map(TailorValidationResult::invalid)
                .orElse(TailorValidationResult.valid());
    }
}
