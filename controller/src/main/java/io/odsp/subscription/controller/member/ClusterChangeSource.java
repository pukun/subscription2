package io.odsp.subscription.controller.member;

import reactor.core.publisher.Flux;

public interface ClusterChangeSource {
    Flux<ClusterChange> observe();
}
