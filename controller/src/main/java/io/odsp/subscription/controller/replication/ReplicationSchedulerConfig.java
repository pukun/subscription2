package io.odsp.subscription.controller.replication;

import io.odsp.subscription.controller.member.ClusterChangeSource;
import io.odsp.subscription.controller.member.ReplicationProxy;
import io.odsp.subscription.controller.subscription.SubscriptionChangeSource;
import io.odsp.subscription.controller.subscription.SubscriptionRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Duration;

@Configuration
public class ReplicationSchedulerConfig {

    @Value("${scheduler.subscription.interval.minutes:10}")
    private int subscriptionScreenInterval;

    @Value("${scheduler.replication.interval.minutes:5")
    private int replicationScreenInterval;

    @Value("${scheduler.node.capacity.limit:100")
    private int nodeCapacityLimit;

    @Bean
    public ReplicationSchedulerContext context(
            ClusterChangeSource clusterChangeSource,
            ReplicationProxy replicationProxy,
            SubscriptionChangeSource subscriptionChangeSource,
            ReplicationBundleRepository repository,
            SubscriptionRepository subscriptionRepository,
            TransactionTemplate tx
    ) {
        return new ReplicationSchedulerContext(
                clusterChangeSource, replicationProxy, subscriptionChangeSource,
                tx, repository, subscriptionRepository,
                Duration.ofMinutes(subscriptionScreenInterval),
                Duration.ofMinutes(replicationScreenInterval),
                nodeCapacityLimit
        );
    }

    @Bean
    public ReplicationScheduler replicator(ReplicationSchedulerContext context) {
        ReplicationScheduler replicator = new ReplicationScheduler(context);

        replicator.initialize();
        return replicator;
    }

}
