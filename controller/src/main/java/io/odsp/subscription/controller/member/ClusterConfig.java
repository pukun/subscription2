package io.odsp.subscription.controller.member;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.support.TransactionTemplate;
import reactor.core.publisher.Sinks;

import java.time.Duration;

@Configuration
public class ClusterConfig {
    private static final Logger logger = LoggerFactory.getLogger("cluster");

    @Value("${cluster.ping.interval: 10}")
    private int pingInterval;

    @Value("${cluster.liveness.threshold: 3}")
    private int livenessThreshold;

    @Value("${cluster.member.timeout:5}")
    private int channelTimeout;

    @Bean
    public MemberChannelFactory channelFactory() {
        return new HttpMemberChannelFactory(channelTimeout);
    }

    @Bean
    public ClusterContext clusterContext(MemberChannelFactory channelFactory, MemberRepository repository, TransactionTemplate tx) {
        logger.info("Initialize cluster context using[ " +
                "pingInterval:{}, livenessThreshold:{}, channelTimeout:{}" +
                "]", pingInterval, livenessThreshold, channelTimeout
        );

        return new ClusterContext(
                channelFactory, repository, tx,
                Sinks.many().multicast().directBestEffort(),
                new FailureDetector(Duration.ofSeconds(pingInterval), livenessThreshold)
        );
    }

    @Bean
    public Cluster cluster(ClusterContext context) {
        return Cluster.build(context);
    }
}
