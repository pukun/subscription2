package io.odsp.subscription.controller.replication;

public enum ReplicationBundleStatus {
    SCHEDULING,
    STARTING,
    RUNNING,
    STOPPING,
    STOPPED
}
