package io.odsp.subscription.controller.subscription.converter;

import io.odsp.subscription.commons.content.TailorExpression;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class TailorExpressionConverter implements AttributeConverter<TailorExpression, String> {
    @Override
    public String convertToDatabaseColumn(TailorExpression attribute) {
        return attribute.exp();
    }

    @Override
    public TailorExpression convertToEntityAttribute(String dbData) {
        return new TailorExpression(dbData);
    }
}
