package io.odsp.subscription.controller.subscription;

import reactor.core.publisher.Flux;

public interface SubscriptionChangeSource {
    Flux<SubscriptionChange> observe();
}
