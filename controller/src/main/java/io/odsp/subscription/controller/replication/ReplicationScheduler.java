package io.odsp.subscription.controller.replication;

import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.controller.member.ClusterChange;
import io.odsp.subscription.controller.subscription.Subscription;
import io.odsp.subscription.controller.subscription.SubscriptionChange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class ReplicationScheduler {
    private static final Logger logger = LoggerFactory.getLogger("replication");

    private final reactor.core.scheduler.Scheduler actor = Schedulers.fromExecutorService(
            Executors.newSingleThreadExecutor(r -> new Thread(r, "replication-scheduler")));

    private final ReplicationBundleFactory factory;
    private final ReplicationSchedulerContext context;
    private final ReplicationSchedule schedule;

    public ReplicationScheduler(ReplicationSchedulerContext context) {
        this.context = context;

        this.factory = new ReplicationBundleFactory();
        this.schedule = context.emptySchedule();
    }

    //region initialization
    void initialize() {
        watchSubscriptionChange();
        watchClusterChange();

        superviseStrandedSubscriptions();
        superviseStrandedReplications();
    }


    //endregion

    //region subscription change
    void watchSubscriptionChange() {
        context.subscriptionChange()
                .subscribeOn(actor)
                .subscribe(
                        this::onSubscriptionChange,
                        t -> logger.error("Unexpected exception raised from subscription source", t)
                );
    }

    void onSubscriptionChange(SubscriptionChange subscriptionChange) {
        SubscriptionId id = subscriptionChange.id();
        onReplicationChanges(subscriptionChange.isActive() ? onSubscribed(id) : onUnsubscribed(id));
    }

    void onSubscriptionChanges(List<? extends SubscriptionChange> subscriptionChanges) {
        Stream<ReplicationChange> unsubscribed = subscriptionChanges.stream()
                .filter(Predicate.not(SubscriptionChange::isActive))
                .map(SubscriptionChange::id)
                .flatMap(id -> onUnsubscribed(id).stream());

        Stream<ReplicationChange> subscribed = subscriptionChanges.stream()
                .filter(SubscriptionChange::isActive)
                .map(SubscriptionChange::id)
                .flatMap(id -> onUnsubscribed(id).stream());

        onReplicationChanges(Stream.concat(unsubscribed, subscribed).toList());
    }

    List<ReplicationChange> onSubscribed(SubscriptionId subscriptionId) {
        return context.subscriptionOf(subscriptionId)
                .filter(Subscription::active)
                .map(s -> schedule.onSubscribed(factory, s))
                .orElse(Collections.emptyList());
    }

    //todo process unsubscribe event
    List<ReplicationChange> onUnsubscribed(SubscriptionId subscriptionId) {
        return Collections.emptyList();
    }

    //endregion

    //region cluster change
    void watchClusterChange() {
        context.clusterChange()
                .subscribeOn(actor)
                .subscribe(
                        this::onClusterChange,
                        t -> logger.error("Unexpected exception raised from cluster", t)
                );
    }

    //todo process member-join and member-left event
    void onClusterChange(ClusterChange clusterChange) {

    }
    //endregion


    //region screen stranded subscriptions
    void superviseStrandedSubscriptions() {
        Flux.interval(Duration.ofSeconds(5), context.subscriptionScreenInterval())
                .subscribeOn(actor)
                .map(i -> Instant.now().minus(context.subscriptionScreenInterval()))
                .subscribe(this::screenStrandedSubscriptions);
    }


    void screenStrandedSubscriptions(Instant checkMoment) {
        onSubscriptionChanges(Stream.concat(
                context.strandedActiveSubscriptions(checkMoment).stream(),
                context.strandedInActiveSubscriptions(checkMoment).stream()
        ).toList());
    }
    //endregion

    //region screen stranded tasks
    void superviseStrandedReplications() {
        Flux.interval(Duration.ofSeconds(10), context.replicationScreenInterval())
                .subscribeOn(actor)
                .subscribe(i -> screenStrandedTasks());
    }

    //todo process bundles at a standstill
    void screenStrandedTasks() {

    }
    //endregion

    //region replication bundle processing
    void onReplicationChanges(List<ReplicationChange> replicationChanges) {
        context.save(replicationChanges);

        invocationsFrom(replicationChanges)
                .forEach((m, rs) -> invoke(m, rs).subscribe(
                        this::onReplicationChanges,
                        t -> logger.info("failed to send replication request to member {}", m, t)
                ));
    }

    Map<MemberId, List<ReplicationRequest>> invocationsFrom(List<ReplicationChange> replicationChanges) {
        Map<MemberId, List<ReplicationRequest>> invocations = new HashMap<>();
        replicationChanges.forEach(change -> change.scheduleOf().ifPresent(s -> invocations.merge(
                s.getKey(),
                Collections.singletonList(s.getValue()),
                (l, r) -> Stream.concat(l.stream(), r.stream()).toList()
        )));

        return invocations;
    }

    Mono<List<ReplicationChange>> invoke(MemberId m, List<ReplicationRequest> rs) {
        return Mono.defer(() -> context.invoke(m, rs))
                .retry(2)
                .timeout(Duration.ofSeconds(5))
                .map(schedule::onResponses)
                .subscribeOn(actor);
    }
    //endregion
}
