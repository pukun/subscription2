package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.persistence.Mutable;
import io.odsp.subscription.commons.protocol.subscription.Destination;
import io.odsp.subscription.commons.protocol.subscription.NamingPattern;
import io.odsp.subscription.commons.protocol.subscription.Source;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.controller.subscription.converter.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.nio.charset.Charset;
import java.time.Instant;

@Entity
@Table(name = "t_subscription")
@Accessors(fluent = true, chain = true)
@Getter
@Setter
public class Subscription extends Mutable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscriptionGenerator")
    @SequenceGenerator(name = "subscriptionGenerator", sequenceName = "seq_subscription", allocationSize = 1)
    private int id;

    @Column(nullable = false, updatable = false)
    private String application;

    @Column(nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private Protocol protocol;

    @Column(nullable = false, updatable = false)
    @Enumerated(EnumType.STRING)
    private Format format;

    @Convert(converter = EncodingConverter.class)
    private Charset encoding;

    @Column(nullable = false, updatable = false)
    @Convert(converter = SourceConverter.class)
    private Source source;

    @Column(nullable = false)
    @Convert(converter = NamingPatternConverter.class)
    private NamingPattern filter;

    @Column(updatable = false)
    @Convert(converter = TailorExpressionConverter.class)
    private TailorExpression tailor;

    @Column(nullable = false, updatable = false)
    @Convert(converter = DestinationConverter.class)
    private Destination destination;

    private boolean active;

    @Column(name = "activated_time", nullable = false, updatable = false)
    private Instant activatedTime;

    @Column(name = "deactivated_time")
    private Instant deactivatedTime;

    public Subscription unsubscribe() {
        if (active) {
            active = false;
        }

        deactivatedTime = Instant.now();
        return this;
    }

    public SubscriptionId sid() {
        return SubscriptionId.of(id);
    }
}
