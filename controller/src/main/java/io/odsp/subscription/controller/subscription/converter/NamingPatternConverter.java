package io.odsp.subscription.controller.subscription.converter;

import io.odsp.subscription.commons.protocol.subscription.NamingPattern;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class NamingPatternConverter implements AttributeConverter<NamingPattern, String> {
    @Override
    public String convertToDatabaseColumn(NamingPattern attribute) {
        return attribute.pattern();
    }

    @Override
    public NamingPattern convertToEntityAttribute(String dbData) {
        return new NamingPattern(dbData);
    }
}
