package io.odsp.subscription.controller.replication;

import io.odsp.subscription.commons.persistence.Mutable;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.replication.ReplicationId;
import io.odsp.subscription.controller.member.EpochConverter;
import io.odsp.subscription.controller.subscription.Subscription;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Optional;

@Entity
@Table(name = "t_replication_bundle")
@Accessors(fluent = true, chain = true)
@Getter
@Setter
public class ReplicationBundle extends Mutable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_replication")
    @SequenceGenerator(name = "seq_replication", allocationSize = 100)
    private int id;

    @ManyToOne
    @JoinColumn(name = "subscription_id")
    private Subscription subscription;

    @Column(name = "sequence", updatable = false)
    private int sequence;

    @Column(name = "size", updatable = false)
    private int size;

    @Enumerated(EnumType.STRING)
    private ReplicationBundleStatus status;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "host", column = @Column(name = "member_host")),
            @AttributeOverride(name = "port", column = @Column(name = "member_port")),
    })
    private Node node;

    @Column(name = "member_epoch")
    @Convert(converter = EpochConverter.class)
    private Epoch epoch;

    //region status transition
    void onScheduled(MemberId member) {
        if (status == ReplicationBundleStatus.SCHEDULING) {
            status = ReplicationBundleStatus.STARTING;
            this.node = member.node();
            this.epoch = member.epoch();
        }
    }

    boolean onStarted() {
        if (status != ReplicationBundleStatus.STARTING) return false;

        status = ReplicationBundleStatus.RUNNING;
        return true;
    }

    public boolean onDismissed() {
        if (status == ReplicationBundleStatus.STOPPING || status == ReplicationBundleStatus.STOPPED) return false;

        if (status == ReplicationBundleStatus.SCHEDULING) status = ReplicationBundleStatus.STOPPED;
        else status = ReplicationBundleStatus.STOPPING;

        return true;
    }

    boolean onStopped() {
        if (status != ReplicationBundleStatus.STOPPING) return false;

        this.status = ReplicationBundleStatus.STOPPED;
        this.node = null;
        this.epoch = null;

        return true;
    }

    public boolean isActive() {
        return status != ReplicationBundleStatus.STOPPED;
    }
    //endregion

    public Optional<MemberId> placement() {
        return Optional.ofNullable(node)
                .flatMap(n -> Optional.ofNullable(epoch)
                        .map(e -> new MemberId(n, e)));
    }

    public ReplicationId rid() {
        return ReplicationId.of(id);
    }

}
