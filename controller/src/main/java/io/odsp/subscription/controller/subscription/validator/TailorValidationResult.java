package io.odsp.subscription.controller.subscription.validator;

public sealed interface TailorValidationResult permits TailorValidationResult.Valid, TailorValidationResult.Invalid {
    static TailorValidationResult valid() {
        return new Valid();
    }

    static TailorValidationResult invalid(String error) {
        return new Invalid(error);
    }

    record Valid() implements TailorValidationResult {
    }

    record Invalid(String error) implements TailorValidationResult {

    }

}
