package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import io.odsp.subscription.commons.protocol.replication.ReplicationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.MonoSink;
import reactor.core.publisher.Sinks;
import reactor.core.scheduler.Schedulers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class Cluster implements ClusterChangeSource, ReplicationProxy {
    private static final Logger logger = LoggerFactory.getLogger("cluster");


    private final ExecutorService actor = Executors.newSingleThreadExecutor(r -> new Thread(r, "cluster-executor"));

    private final ClusterContext context;

    private final Map<Node, Member> members = new ConcurrentHashMap<>();
    private final Map<MemberId, Disposable> watches = new HashMap<>();


    //region builder
    Cluster(ClusterContext context) {
        this.context = context;
    }

    public static Cluster build(ClusterContext context) {
        Cluster cluster = new Cluster(context);

        context.loadAllActiveMembers().forEach(cluster::assimilate);

        return cluster;
    }
    //endregion

    //region membership
    @Override
    public Flux<ClusterChange> observe() {
        return Flux.concat(
                Mono.just(ClusterChange.clusterBuilt(members())),
                context.fluxOfChange()
        );
    }

    Mono<MemberId> onJoinRequest(Node node) {
        return Mono.create(sink -> actor.execute(() -> this.onJoinRequest(node, sink)));
    }

    List<MemberId> members() {
        return members.values().stream()
                .map(Member::id).collect(Collectors.toList());
    }

    Optional<MemberId> memberOf(Node node) {
        return Optional.ofNullable(members.get(node))
                .map(Member::id);
    }

    private void onJoinRequest(Node node, MonoSink<MemberId> sink) {
        try {
            sink.success(Optional.ofNullable(members.get(node))
                    .or(() -> context.historyMemberOf(node))
                    .map(this::age)
                    .orElseGet(() -> this.join(node))
            );
        } catch (Throwable t) {
            logger.info("Unexpected exception raised when [{}] attempts to join", node, t);
            sink.error(t);
        }
    }

    private MemberId age(Member currentMember) {
        if (currentMember.isUp()) disperse(currentMember);

        Member agedMember = context.age(currentMember.id());

        assimilate(agedMember);
        context.emitChangeOfAgingMember(currentMember.id(), agedMember.epoch());

        return agedMember.id();
    }

    private MemberId join(Node joiningNode) {
        Member member = context.create(joiningNode);

        assimilate(member);
        context.emitChangeOfNewMember(member);

        return member.id();
    }

    private void assimilate(Member member) {
        members.put(member.node(), member);
        watches.put(member.id(), watch(member));
    }

    private void disperse(Member member) {
        members.remove(member.node());
        Optional.ofNullable(watches.remove(member.id()))
                .ifPresent(Disposable::dispose);

        member.leave();
    }

    private Disposable watch(Member member) {
        return context.watch(member)
                .subscribeOn(Schedulers.fromExecutorService(actor))
                .subscribe(
                        f -> onFailureDetected(member),
                        t -> onFailureDetected(member, t)
                );
    }

    private void onFailureDetected(Member member) {
        onFailureDetected(member, null);
    }

    private void onFailureDetected(Member member, Throwable t) {
        if (!members.containsKey(member.id().node())) return;
        if (t != null) logger.info("Exception thrown in failure detector for ({})", member, t);

        disperse(member);

        context.leave(member);

        context.emitChangeOfDispersedMember(member);
    }

    //endregion

    //region replication
    @Override
    public Flux<ReplicationResponse> replicate(MemberId memberId, List<ReplicationRequest> requests) {
        Sinks.Many<ReplicationResponse> responses = Sinks.many().unicast().onBackpressureBuffer();

        actor.execute(() -> replicate(memberId, requests, responses));

        return responses.asFlux();
    }

    private void replicate(MemberId memberId, List<ReplicationRequest> requests, Sinks.Many<ReplicationResponse> responses) {
        Member member = members.get(memberId.node());
        if (member == null) {
            responses.tryEmitError(new RuntimeException(String.format("No such member(%s)", memberId)));
            return;
        }

        try {
            member.replicate(requests)
                    .subscribe(responses::tryEmitNext, responses::tryEmitError, responses::tryEmitComplete);
        } catch (Throwable t) {
            responses.tryEmitError(t);
        }
    }
    //endregion
}
