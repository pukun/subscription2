package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;

public sealed interface SubscriptionChange permits SubscriptionChange.SubscriptionActivation, SubscriptionChange.SubscriptionDeactivation {
    SubscriptionId id();

    default boolean isActive() {
        return false;
    }

    static SubscriptionActivation subscribed(SubscriptionId id) {
        return new SubscriptionActivation(id);
    }

    record SubscriptionActivation(SubscriptionId id) implements SubscriptionChange {
        @Override
        public boolean isActive() {
            return true;
        }
    }

    static SubscriptionDeactivation unsubscribed(SubscriptionId id) {
        return new SubscriptionDeactivation(id);
    }

    record SubscriptionDeactivation(SubscriptionId id) implements SubscriptionChange {
    }
}
