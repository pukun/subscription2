package io.odsp.subscription.controller.subscription.validator;

import io.odsp.subscription.commons.content.Protocol;

import java.io.File;

public final class CephFSValidator implements AccessibilityValidator {
    @Override
    public Protocol protocol() {
        return Protocol.CEPH_FS;
    }

    @Override
    public AccessibilityValidationResult test(String url) {
        File file = new File(url);
        try {
            return new AccessibilityValidationResult(file.isDirectory(), file.canRead(), file.canWrite());
        } catch (Throwable t) {
            return new AccessibilityValidationResult(true, false, false);
        }
    }

}
