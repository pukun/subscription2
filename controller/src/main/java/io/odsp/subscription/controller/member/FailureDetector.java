package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.PingResponse;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

@Accessors(fluent = true)
@Getter
class FailureDetector {
    private static final Logger logger = LoggerFactory.getLogger("cluster");

    private final Duration pingInterval;
    private final int livenessThreshold;

    private final Map<Member, Integer> failures = new ConcurrentHashMap<>();

    FailureDetector(Duration pingInterval, int livenessThreshold) {
        this.pingInterval = pingInterval;
        this.livenessThreshold = livenessThreshold;
    }

    Mono<Boolean> watch(Member member) {
        return detect(member)
                .doOnSubscribe(s -> failures.putIfAbsent(member, 0))
                .doFinally(s -> failures.remove(member));
    }


    private Mono<Boolean> detect(Member member) {
        Predicate<Boolean> hasMemberLeft = pong -> this.hasMemberLeft(member, pong);

        return Flux.interval(pingInterval, pingInterval)
                .flatMap(i -> ping(member))
                .filter(hasMemberLeft)
                .next();
    }

    private Mono<Boolean> ping(Member member) {
        try {
            return member.ping()
                    .map(PingResponse::isOk)
                    .onErrorReturn(false);
        } catch (Throwable t) {
            logger.info("Unexpected error when attempting to ping member({})", member.id(), t);
            return Mono.just(false);
        }
    }

    private boolean hasMemberLeft(Member member, boolean pinged) {
        return failures.compute(member, (m, f) -> pinged ? 0 : f + 1) >= livenessThreshold;

    }
}