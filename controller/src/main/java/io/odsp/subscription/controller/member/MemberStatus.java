package io.odsp.subscription.controller.member;

public enum MemberStatus {
    UP,
    DOWN
}
