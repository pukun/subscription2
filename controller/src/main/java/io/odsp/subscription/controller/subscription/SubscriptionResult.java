package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.controller.subscription.validator.ValidationError;

public sealed interface SubscriptionResult permits SubscriptionResult.Subscribed, SubscriptionResult.IllegalSpec, SubscriptionResult.UnknownException {

    static SubscriptionResult subscribed(Subscription subscription) {
        return new Subscribed(subscription);
    }

    record Subscribed(Subscription subscription) implements SubscriptionResult {
    }

    static SubscriptionResult illegalSpec(ValidationError error, String detail) {
        return new IllegalSpec(String.format("%s(%s)", error.name(), detail));
    }

    record IllegalSpec(String detail) implements SubscriptionResult {
    }

    static SubscriptionResult unknownException(String detail) {
        return new UnknownException(detail);
    }

    record UnknownException(String detail) implements SubscriptionResult {
    }

}
