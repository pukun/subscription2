package io.odsp.subscription.controller.subscription.validator;

public record AccessibilityValidationResult(boolean invalidURI, boolean canRead, boolean canWrite) {

}
