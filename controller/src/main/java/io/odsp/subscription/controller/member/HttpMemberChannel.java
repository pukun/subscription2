package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import io.odsp.subscription.commons.protocol.replication.ReplicationResponse;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public record HttpMemberChannel(MemberId id, WebClient client) implements MemberChannel {

    @Override
    public Mono<PingResponse> ping() {
        return client.post().uri(Endpoints.ping(id.epoch()))
                .contentType(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToMono(PingResponse.class);
    }

    @Override
    public Flux<ReplicationResponse> replicate(List<ReplicationRequest> replicationRequests) {
        return client.post()
                .uri(Endpoints.replicate(id.epoch()))
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(replicationRequests)
                .retrieve()
                .bodyToFlux(ReplicationResponse.class);
    }

}
