package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import io.odsp.subscription.commons.protocol.replication.ReplicationResponse;
import reactor.core.publisher.Flux;

import java.util.List;

public interface ReplicationProxy {
    Flux<ReplicationResponse> replicate(MemberId memberId, List<ReplicationRequest> requests);
}
