package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.protocol.subscription.Destination;
import io.odsp.subscription.commons.protocol.subscription.Source;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionSpec;
import io.odsp.subscription.controller.subscription.validator.AccessibilityValidationResult;
import io.odsp.subscription.controller.subscription.validator.AccessibilityValidators;
import io.odsp.subscription.controller.subscription.validator.TailorValidationResult;
import io.odsp.subscription.controller.subscription.validator.TailorValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.time.Instant;
import java.util.Optional;

import static io.odsp.subscription.controller.subscription.validator.ValidationError.*;

public record SubscriptionFactory(SubscriptionRepository repository) {
    private static final Logger logger = LoggerFactory.getLogger(SubscriptionFactory.class);

    SubscriptionResult create(final SubscriptionSpec spec) {
        return verifySourceCanBeAccessed(spec)
                .or(() -> verifyDestinationCanBeAccessed(spec))
                .or(() -> validateTailor(spec))
                .or(() -> validateEncoding(spec))
                .orElse(SubscriptionResult.subscribed(build(spec)));
    }


    Optional<SubscriptionResult> verifySourceCanBeAccessed(SubscriptionSpec spec) {
        try {
            return checkAccessibility(spec.protocol(), spec.source())
                    .map(error -> SubscriptionResult.illegalSpec(InaccessibleSource, error));
        } catch (IllegalArgumentException iae) {
            return Optional.of(SubscriptionResult.illegalSpec(UnknownProtocol, iae.getMessage()));
        }
    }

    Optional<SubscriptionResult> verifyDestinationCanBeAccessed(SubscriptionSpec spec) {
        try {
            return checkAccessibility(spec.protocol(), spec.destination())
                    .map(error -> SubscriptionResult.illegalSpec(InaccessibleDestination, error));
        } catch (IllegalArgumentException iae) {
            return Optional.of(SubscriptionResult.illegalSpec(UnknownProtocol, iae.getMessage()));
        }
    }

    Optional<SubscriptionResult> validateTailor(SubscriptionSpec spec) {
        try {
            TailorValidationResult tailorValidationResult = TailorValidator.test(spec.format(), spec.tailor().exp());
            if (tailorValidationResult instanceof TailorValidationResult.Invalid invalidTailor)
                return Optional.of(SubscriptionResult.illegalSpec(InvalidTailor, invalidTailor.error()));
            else return Optional.empty();
        } catch (IllegalArgumentException iae) {
            return Optional.of(SubscriptionResult.illegalSpec(UnknownFormat, iae.getMessage()));
        }
    }

    Optional<SubscriptionResult> validateEncoding(SubscriptionSpec spec) {
        try {
            Charset.forName(spec.encoding());
            return Optional.empty();
        } catch (Throwable t) {
            return Optional.of(SubscriptionResult.illegalSpec(UnknownEncoding, ""));
        }
    }

    public Subscription build(SubscriptionSpec spec) {
        Subscription subscription = new Subscription();

        return subscription.application(spec.application())
                .protocol(spec.protocol())
                .format(spec.format())
                .encoding(Charset.forName(spec.encoding()))
                .source(spec.source())
                .filter(spec.filter())
                .tailor(spec.tailor())
                .destination(spec.destination())
                .activatedTime(Instant.now())
                .active(true);
    }

    private Optional<String> checkAccessibility(Protocol protocol, Source source) {
        return checkAccessibility(protocol, source.path());
    }

    private Optional<String> checkAccessibility(Protocol protocol, Destination destination) {
        return checkAccessibility(protocol, destination.path());
    }

    private Optional<String> checkAccessibility(Protocol protocol, String uri) {
        try {
            AccessibilityValidationResult accessibilityValidationResult = AccessibilityValidators.check(protocol, uri);
            if (accessibilityValidationResult.invalidURI()) return Optional.of("invalid URI");
            else if (!accessibilityValidationResult.canRead()) return Optional.of("can not read");
            else if (!accessibilityValidationResult.canWrite()) return Optional.of("can not modify/delete");
        } catch (Throwable t) {
            return Optional.of(t.getMessage());
        }

        return Optional.empty();
    }
}