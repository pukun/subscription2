package io.odsp.subscription.controller.replication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Instant;
import java.util.List;

public interface ReplicationBundleRepository extends JpaRepository<ReplicationBundle, Integer> {
    @Query("""
            select distinct(s.id) from Subscription s where
            s.active = TRUE and s.activatedTime <= :screenTime
            and not exists(select 1 from ReplicationBundle t where t.subscription = s)
            """
    )
    List<Integer> findActiveSubscriptionsHaveNotBeenStarted(@Param("screenTime") Instant screenTime);

    @Query("""
            select distinct(s.id) from Subscription s where
            s.active = FALSE and s.deactivatedTime <= :screenTime
            and exists(
            select 1 from ReplicationBundle t where t.subscription = s
            and t.status != io.odsp.subscription.controller.replication.ReplicationBundleStatus.STOPPED
            )
            """
    )
    List<Integer> findInActiveSubscriptionsHaveNotBeenStopped(@Param("screenTime") Instant screenTime);
}
