package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import io.odsp.subscription.commons.protocol.replication.ReplicationResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface MemberChannel {
    MemberId id();

    Mono<PingResponse> ping();

    Flux<ReplicationResponse> replicate(List<ReplicationRequest> replicationRequests);
}
