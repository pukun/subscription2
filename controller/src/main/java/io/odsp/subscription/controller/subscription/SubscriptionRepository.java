package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.protocol.subscription.Source;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.nio.charset.Charset;
import java.util.Optional;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Integer> {
    @Modifying
    @Query(value = """
            INSERT INTO t_subscription(
            id, application,
            protocol, format, encoding, source,
            filter, tailor,
            destination,
            active, activated_time,
            created_time, last_modified_time, version
            ) values (
            NEXTVAL('seq_subscription'), :#{#spec.application},
            :#{#spec.protocol.name()}, :#{#spec.format.name()}, :#{#spec.encoding.name()}, :#{#spec.source.path()},
            :#{#spec.filter.pattern()}, :#{#spec.tailor.exp()},
            :#{#spec.destination.path()},
            TRUE, NOW(),
            NOW(), NOW(), 1
            )
            ON CONFLICT(application, protocol, format, encoding, source)
            WHERE active=TRUE
            DO NOTHING
            """,
            nativeQuery = true
    )
    void putIfAbsent(@Param("spec") Subscription subscription);

    @Query("""
            select s from #{#entityName} s where
            s.application = :app and s.protocol = :protocol and s.format = :format
            and s.encoding = :encoding and s.source = :source and s.active = TRUE
            """)
    Optional<Subscription> findActiveSubscription(
            @Param("app") String app,
            @Param("protocol") Protocol protocol, @Param("source") Source source,
            @Param("format") Format format, @Param("encoding") Charset encoding
    );

    default Optional<Subscription> findActiveSubscription(Subscription subscription) {
        return findActiveSubscription(
                subscription.application(),
                subscription.protocol(), subscription.source(),
                subscription.format(), subscription.encoding()
        );
    }

    @Query("select s from #{#entityName} s where s.active = TRUE and s.id = :#{#subscriptionId.id()}")
    Optional<Subscription> findActiveSubscriptionById(@Param("subscriptionId") SubscriptionId subscriptionId);
}