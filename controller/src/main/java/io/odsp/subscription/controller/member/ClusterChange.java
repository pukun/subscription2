package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;

import java.util.List;

import static io.odsp.subscription.controller.member.ClusterChange.*;

public sealed interface ClusterChange permits ClusterBuilt, MemberJoined, MemberLeft, MemberAged {

    static ClusterBuilt clusterBuilt(List<MemberId> members) {
        return new ClusterBuilt(members);
    }

    record ClusterBuilt(List<MemberId> members) implements ClusterChange {
    }

    static MemberJoined memberJoined(MemberId member) {
        return new MemberJoined(member);
    }

    record MemberJoined(MemberId member) implements ClusterChange {
    }

    static MemberLeft memberLeft(MemberId member) {
        return new MemberLeft(member);
    }

    record MemberLeft(MemberId member) implements ClusterChange {
    }

    static MemberAged memberAged(MemberId member, Epoch current) {
        return new MemberAged(member, current);
    }

    record MemberAged(MemberId member, Epoch current) implements ClusterChange {
    }
}
