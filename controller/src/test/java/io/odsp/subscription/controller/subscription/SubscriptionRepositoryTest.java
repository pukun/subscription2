package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.protocol.subscription.Destination;
import io.odsp.subscription.commons.protocol.subscription.NamingPattern;
import io.odsp.subscription.commons.protocol.subscription.Source;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionSpec;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.ActiveProfiles;

import java.nio.charset.Charset;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@ActiveProfiles("docker")
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@EnableJpaAuditing
public class SubscriptionRepositoryTest {
    @Autowired
    private SubscriptionRepository repository;


    @Test
    public void subscriptionOnlyActivatedOnce() {
        SubscriptionSpec spec = new SubscriptionSpec(
                "app",
                Protocol.CEPH_FS, Format.JSON, Charset.defaultCharset().name(),
                Source.of("source"), NamingPattern.of("**"), TailorExpression.of(""), Destination.of("dest")
        );

        Subscription subscription = new SubscriptionFactory(repository).build(spec);

        //When we subscribe for the first time
        assertThat(repository.count(), is(0L));
        repository.putIfAbsent(subscription);

        //Then subscriptionSpec succeeds
        Optional<Subscription> activated = repository.findActiveSubscription(
                subscription.application(),
                subscription.protocol(), subscription.source(),
                subscription.format(), subscription.encoding()
        );
        assertThat(activated.isPresent(), is(true));

        //When we attempt to subscribe it again
        repository.putIfAbsent(subscription);

        //No new subscriptionSpec will be added since it's been subscribed
        assertThat(repository.count(), is(1L));

        //If we unsubscribe the active subscriptionSpec
        activated.map(Subscription::unsubscribe).ifPresent(repository::save);
        assertThat(repository.findActiveSubscription(subscription).isEmpty(), is(true));

        //Then subscriptionSpec can be activated as another instance.
        repository.putIfAbsent(subscription);
        assertThat(repository.count(), is(2L));
    }
}
