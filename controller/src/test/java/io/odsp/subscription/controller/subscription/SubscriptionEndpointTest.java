package io.odsp.subscription.controller.subscription;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.protocol.subscription.*;
import io.odsp.subscription.controller.subscription.validator.ValidationError;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static io.odsp.subscription.controller.subscription.SubscriptionResult.illegalSpec;
import static org.mockito.Mockito.when;

@WebFluxTest(SubscriptionEndPoint.class)
public class SubscriptionEndpointTest {

    @Autowired
    private WebTestClient client;

    @MockBean
    private SubscriptionService service;

    private final SubscriptionSpec subscriptionSpec = new SubscriptionSpec(
            "app",
            Protocol.CEPH_FS, Format.XML, StandardCharsets.UTF_8.name(),
            Source.of("source"), NamingPattern.of("filter"), TailorExpression.of("tailor"),
            Destination.of("destination")
    );
    private final int subscriptionId = 100;


    @Test
    public void givenInvalidSpecThenSubscriptionFail() {
        when(service.subscribe(subscriptionSpec))
                .thenReturn(illegalSpec(ValidationError.InaccessibleSource, "can't read"));

        WebTestClient.BodyContentSpec bodyContentSpec = subscribe()
                .expectStatus().isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
                .expectBody();

        bodyContentSpec.jsonPath("$.code").isEqualTo("IllegalSpec");

    }

    @Test
    public void givenSubscriptionNotFoundThenTerminationFail() {
        when(service.unsubscribe(SubscriptionId.of(subscriptionId)))
                .thenReturn(Optional.empty());

        unsubscribe()
                .expectStatus().isNotFound()
                .expectBody().isEmpty();
    }

    private WebTestClient.ResponseSpec subscribe() {
        return client.post()
                .uri("/v1/subscriptions")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(subscriptionSpec)
                .exchange();
    }

    private WebTestClient.ResponseSpec unsubscribe() {
        return client.post()
                .uri(String.format("/v1/subscriptions/%d/unsubscribe", subscriptionId))
                .exchange();

    }
}
