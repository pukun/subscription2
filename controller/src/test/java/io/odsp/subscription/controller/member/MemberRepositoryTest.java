package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.Node;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;

@ActiveProfiles("docker")
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@EnableJpaAuditing
public class MemberRepositoryTest {
    @Autowired
    private MemberRepository repository;

    @Test
    public void load_members() {
        Node localNode = Node.builder().host("local").port(1010).build();
        Epoch epoch = new Epoch(3);
        Member member = new Member()
                .node(localNode).epoch(epoch)
                .status(MemberStatus.DOWN);

        repository.save(member);

        assertThat(repository.getAllActiveMembers(), empty());
        assertThat(repository.findById(localNode), equalTo(Optional.of(member)));
    }
}
