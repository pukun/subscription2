package io.odsp.subscription.controller;

import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest
@ActiveProfiles("docker")
@AutoConfigureWebTestClient
public class ControllerAppTest {
    @Test
    public void appTest(@Autowired WebTestClient client) {
        client.get().uri(Endpoints.CLUSTER)
                .exchange()
                .expectBodyList(MemberId.class)
                .hasSize(0);
    }
}
