package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.Error;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import static org.mockito.BDDMockito.given;


@WebFluxTest(ClusterEndpoint.class)
public class ClusterEndPointTest {
    @Autowired
    private WebTestClient client;

    @MockBean
    private Cluster cluster;

    private final Node node = Node.builder().host("h").port(80).build();

    private final MemberId id = new MemberId(node, Epoch.first());

    @Test
    public void given_join_succeeded_then_response_is_ok() {
        given(cluster.onJoinRequest(node)).willReturn(Mono.just(id));

        client.post().uri(Endpoints.join())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(node)
                .exchange()
                .expectStatus().isOk()
                .expectBody(MemberId.class).isEqualTo(id);
    }

    @Test
    public void given_join_failed_then_response_is_internal_failure() {
        given(cluster.onJoinRequest(node)).willReturn(Mono.error(new RuntimeException("test")));

        client.post().uri(Endpoints.join())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(node)
                .exchange()
                .expectStatus().is5xxServerError()
                .expectBody(Error.class).isEqualTo(new Error("UnexpectedError", "test"));
    }


}
