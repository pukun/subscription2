package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.replication.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import reactor.test.StepVerifier;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class ClusterTest {
    private Cluster cluster;

    private ClusterContext context;

    private final Node w1 = Node.builder().host("h1").port(80).build();
    private final Node w2 = Node.builder().host("h2").port(80).build();
    private final Member m1 = new Member().node(w1).epoch(Epoch.first().next());
    private final Member m2 = new Member().node(w2).epoch(new Epoch(3));

    @BeforeEach
    public void setUp() {
        context = mock(ClusterContext.class);

        Sinks.One<Boolean> s = Sinks.one();
        when(context.watch(any(Member.class))).thenReturn(s.asMono());
    }

    @Nested
    @DisplayName("member")
    class MemberTest {
        @Test
        public void when_observed_cluster_can_emit_snapshot_and_following_changes() {

            //given initially cluster contains m1, m2
            when(context.loadAllActiveMembers()).thenReturn(Arrays.asList(m1, m2));
            Cluster cluster = Cluster.build(context);
            assertThat(cluster.members(), containsInAnyOrder(m1.id(), m2.id()));

            //and context contains a stream of a change event
            when(context.fluxOfChange()).thenReturn(Flux.just(ClusterChange.memberLeft(m2.id())));

            //when we observe
            Flux<ClusterChange> observer = cluster.observe();

            //then all changes can be caught
            StepVerifier.create(observer)
                    .expectSubscription()
                    .expectNext(ClusterChange.clusterBuilt(Arrays.asList(m1.id(), m2.id())))
                    .expectNext(ClusterChange.memberLeft(m2.id()))
                    .verifyComplete();
        }

        @Test
        public void when_new_member_join_then_it_can_be_assimilated() {
            //given an empty cluster
            when(context.loadAllActiveMembers()).thenReturn(Collections.emptyList());
            Cluster cluster = Cluster.build(context);

            //when w1 joins
            Member m = new Member().node(w1).epoch(Epoch.first());
            when(context.create(w1)).thenReturn(m);
            Mono<MemberId> joined = cluster.onJoinRequest(w1);

            //then a new member is created
            assertThat(joined.block(), equalTo(m.id()));

            //and cluster now contains one member
            assertThat(cluster.members().size(), is(1));

            //and a change event is emitted
            verify(context).emitChangeOfNewMember(m);
        }

        @Test
        public void when_old_member_join_again_then_it_ages() {
            Cluster cluster = Cluster.build(context);

            //given m1 is in the cluster
            when(context.historyMemberOf(m1.node())).thenReturn(Optional.of(m1));

            //and m is the elder m1
            Member m = Member.age(m1.node(), m1.epoch(), mock(MemberChannelFactory.class));
            when(context.age(m1.id())).thenReturn(m);

            //when m1 joins again
            Mono<MemberId> joined = cluster.onJoinRequest(m1.node());

            //then we get the elder member
            assertThat(joined.block(), equalTo(m.id()));

            //and a change event is emitted
            verify(context).emitChangeOfAgingMember(m1.id(), m.epoch());
        }
    }

    @Nested
    @DisplayName("proxy")
    class ProxyTest {

        private final List<ReplicationRequest> requests = Arrays.asList(
                ReplicationRequest.start(new Replication(ReplicationId.of(100), mock(Subscription.class), Replication.TOTAL_PARTITIONS, 10, 19)),
                ReplicationRequest.start(new Replication(ReplicationId.of(101), mock(Subscription.class), Replication.TOTAL_PARTITIONS, 80, 90))
        );

        @Test
        public void when_request_sent_to_an_offline_member_then_it_is_rejected() {
            //given there is not any active member in the cluster
            when(context.loadAllActiveMembers()).thenReturn(Collections.emptyList());
            Cluster cluster = Cluster.build(context);

            //send the requests to m1
            Flux<ReplicationResponse> responses = cluster.replicate(m1.id(), requests);

            StepVerifier.create(responses)
                    .expectSubscription()
                    .expectErrorMessage(String.format("No such member(%s)", m1.id()));
        }

    }
}
