package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import io.odsp.subscription.commons.protocol.replication.*;
import io.odsp.subscription.commons.protocol.subscription.*;
import io.odsp.subscription.controller.replication.ReplicationBundle;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.function.Consumer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class HttpMemberChannelTest {
    private MockWebServer server;
    private HttpMemberChannel channel;

    private final MemberId memberId = new MemberId(Node.builder().host("h").port(80).build(), Epoch.first());

    private final HttpMemberChannelFactory factory = new HttpMemberChannelFactory(1);

    private final SubscriptionSpec subscriptionSpec = new SubscriptionSpec(
            "app",
            Protocol.CEPH_FS, Format.XML, StandardCharsets.UTF_8.name(),
            Source.of("source"), NamingPattern.of("filter"), TailorExpression.of("tailor"),
            Destination.of("destination")
    );

    private final Subscription subscription = new Subscription(SubscriptionId.of(2100), subscriptionSpec);

    @BeforeEach
    void setUp() {
        server = new MockWebServer();
        channel = factory.channelOf(memberId, server.url("/").toString());
    }

    @AfterEach
    void shutdown() throws IOException {
        server.shutdown();
    }

    @Test
    public void ping() {
        server.enqueue(new MockResponse()
                .setHeader("Content-Type", "application/json")
                .setBody("""
                        {"response": "pong"}
                        """)
        );

        Mono<PingResponse> pingResponse = channel.ping();

        StepVerifier.create(pingResponse)
                .expectNext(PingResponse.pong())
                .expectComplete()
                .verify();

        expectRequest(request -> {
            assertThat(request.getPath(), is(Endpoints.ping(memberId.epoch())));
        });
    }

    @Test
    public void replicate() {
        //given the response is mixed with 'accepted' and 'rejected'
        server.enqueue(new MockResponse()
                .setHeader("Content-Type", "application/json")
                .setBody("""
                        [
                        {"result": "started", "replication": {"id": 100} },
                        {"result": "rejected", "replication": {"id": 101}, "code": "39", "detail": "NA"}
                        ]
                        """)
        );

        //when send out a request
        Flux<ReplicationResponse> responses = channel.replicate(Arrays.asList(
                ReplicationRequest.start(new Replication(ReplicationId.of(100), subscription, Replication.TOTAL_PARTITIONS, 10, 19)),
                ReplicationRequest.start(new Replication(ReplicationId.of(101), subscription, Replication.TOTAL_PARTITIONS, 80, 90))
        ));

        //then server responds as expected
        StepVerifier.create(responses)
                .expectNext(
                        ReplicationResponse.start(ReplicationId.of(100)),
                        ReplicationResponse.reject(ReplicationId.of(101), "39", "NA")
                )
                .verifyComplete();

    }

    private void expectRequest(Consumer<RecordedRequest> consumer) {
        try {
            consumer.accept(server.takeRequest());
        } catch (InterruptedException ie) {
            throw new IllegalStateException(ie);
        }
    }
}
