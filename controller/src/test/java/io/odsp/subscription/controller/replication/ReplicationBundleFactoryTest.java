package io.odsp.subscription.controller.replication;

import io.odsp.subscription.controller.subscription.Subscription;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.mock;

public class ReplicationBundleFactoryTest {
    private final Subscription subscription = mock(Subscription.class);

    @Test
    public void createBundles() {
        ReplicationBundleFactory factory = new ReplicationBundleFactory(30);
        List<ReplicationBundle> bundles = factory.replicationsFor(subscription);

        assertThat(bundles.size(), is(4));
        assertThat(bundles.stream().map(ReplicationBundle::sequence).collect(Collectors.toList()),
                contains(0, 1, 2, 3));
    }
}
