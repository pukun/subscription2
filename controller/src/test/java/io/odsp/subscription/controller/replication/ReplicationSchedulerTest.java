package io.odsp.subscription.controller.replication;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.replication.*;
import io.odsp.subscription.commons.protocol.subscription.*;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

public class ReplicationSchedulerTest {
    private ReplicationSchedulerContext context;

    private final SubscriptionSpec spec = new SubscriptionSpec(
            "app",
            Protocol.CEPH_FS, Format.JSON, Charset.defaultCharset().name(),
            Source.of("source"), NamingPattern.of("**"), TailorExpression.of(""), Destination.of("dest")
    );

    private final Replication replication = new Replication(ReplicationId.of(100), new Subscription(SubscriptionId.of(10), spec), 100, 10, 19);

    private final Node n1 = new Node("h1", 1010);
    private final MemberId m1 = new MemberId(n1, Epoch.first());
    private final Node n2 = new Node("h2", 1010);
    private final MemberId m2 = new MemberId(n2, Epoch.first());

    private ReplicationScheduler scheduler;
    private final ReplicationSchedule schedule = mock(ReplicationSchedule.class);

    @BeforeEach
    public void setUp() {
        context = mock(ReplicationSchedulerContext.class);
        when(context.emptySchedule()).thenReturn(schedule);
        scheduler = new ReplicationScheduler(context);
    }

    @Nested
    @DisplayName("handle replication changes and invocation responses")
    public class InvocationTests {
        private final ReplicationChange change2 = mock(ReplicationChange.Scheduled.class);

        private final ReplicationChange change3 = mock(ReplicationChange.Dismissed.class);

        private final ReplicationRequest.Start startRequest = ReplicationRequest.start(replication);
        private final ReplicationRequest.Stop stopRequest = ReplicationRequest.stop(ReplicationId.of(30));

        private final List<ReplicationRequest> requests = List.of(startRequest, stopRequest);

        private final ReplicationResponse.Started startResponse = ReplicationResponse.start(startRequest.replication().id());
        private final ReplicationResponse.Stopped stopResponse = ReplicationResponse.stop(stopRequest.replicationId());

        @BeforeEach
        void setUp() {
            when(change2.scheduleOf()).thenReturn(Optional.of(Pair.of(m1, startRequest)));

            when(change3.scheduleOf()).thenReturn(Optional.of(Pair.of(m1, stopRequest)));
        }

        @Test
        public void given_replication_changes_then_generate_invocations() {
            ReplicationChange change1 = mock(ReplicationChange.Stopped.class);
            when(change1.scheduleOf()).thenReturn(Optional.empty());

            ReplicationChange change4 = mock(ReplicationChange.Dismissed.class);
            when(change4.scheduleOf()).thenReturn(Optional.of(Pair.of(m2, ReplicationRequest.stop(ReplicationId.of(999)))));

            Map<MemberId, List<ReplicationRequest>> invocations = scheduler.invocationsFrom(Arrays.asList(change1, change2, change3, change4));

            assertThat(invocations.get(m1), contains(startRequest, stopRequest));
            assertThat(invocations.get(m2), contains(ReplicationRequest.stop(ReplicationId.of(999))));
        }

        @Test
        public void when_invocation_succeeds_then_responses_will_be_processed() {
            List<ReplicationResponse> responses = List.of(startResponse, stopResponse);

            when(context.invoke(m1, requests)).thenReturn(Mono.just(responses));
            when(schedule.onResponses(responses)).thenReturn(Collections.emptyList());

            StepVerifier.create(scheduler.invoke(m1, requests))
                    .expectSubscription()
                    .expectNext(Collections.emptyList())
                    .expectComplete()
                    .verify();

            verify(context).invoke(m1, requests);
        }

        @Test
        public void when_invocation_fails_then_request_will_be_retried_twice() {
            when(context.invoke(m1, requests)).thenReturn(Mono.error(new RuntimeException("test")));

            StepVerifier.create(scheduler.invoke(m1, requests))
                    .expectSubscription()
                    .expectErrorMessage("test")
                    .verify();

            verify(context, times(3)).invoke(m1, requests);
        }
    }
}
