package io.odsp.subscription.controller.replication;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReplicationMapperTest {
    @Test
    @DisplayName("map bundle to replication")
    public void mapBundle(){
        ReplicationBundle bundle = mock(ReplicationBundle.class);

        when(bundle.sequence()).thenReturn(3);
        when(bundle.size()).thenReturn(30);
        assertThat(ReplicationMapper.rangeOf(bundle), is(Pair.of(90, 99)));

        when(bundle.sequence()).thenReturn(2);
        assertThat(ReplicationMapper.rangeOf(bundle), is(Pair.of(60, 89)));
    }
}
