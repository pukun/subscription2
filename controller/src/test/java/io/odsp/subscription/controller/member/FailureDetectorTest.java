package io.odsp.subscription.controller.member;

import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import io.odsp.subscription.commons.protocol.membership.Node;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class FailureDetectorTest {

    private final FailureDetector detector = new FailureDetector(Duration.ofMillis(100), 3);

    private Member member;

    @BeforeEach
    public void setUp() {
        member = mock(Member.class);

        when(member.node()).thenReturn(Node.builder().host("host").port(1080).build());
        when(member.epoch()).thenReturn(Epoch.first().next());
    }

    @Test
    public void when_member_stay_alive_then_watch_never_stop() {
        when(member.ping()).thenReturn(Mono.just(PingResponse.pong()));

        Duration wait = detector.pingInterval().multipliedBy(detector.livenessThreshold() + 1);

        StepVerifier.withVirtualTime(() -> detector.watch(member))
                .expectSubscription()
                .expectNoEvent(wait)
                .thenCancel()
                .verify();

        verify(member, times(detector.livenessThreshold() + 1)).ping();
        assertThat(detector.failures().containsKey(member), is(false));
    }

    @Test
    public void when_member_can_not_be_pinged_then_failure_could_be_detected() {
        given(member.ping()).willThrow(new RuntimeException("null"));

        StepVerifier.create(detector.watch(member))
                .expectSubscription()
                .expectNoEvent(detector.pingInterval())
                .expectNext(false)
                .verifyComplete();

        assertThat(detector.failures().containsKey(member), is(false));
    }

    @Test
    public void when_member_leaves_cluster_then_failure_could_be_detected() {
        when(member.ping()).thenReturn(Mono.just(PingResponse.noMembership()));

        StepVerifier.create(detector.watch(member))
                .expectSubscription()
                .expectNoEvent(detector.pingInterval())
                .expectNext(false)
                .verifyComplete();

        assertThat(detector.failures().containsKey(member), is(false));
    }

    @Test
    public void when_member_recover_before_failure_been_detected_then_watch_will_continue() {
        final AtomicInteger invocation = new AtomicInteger();
        when(member.ping()).thenAnswer(invocationOnMock -> {
            int round = invocation.incrementAndGet();

            if (round == 1) throw new RuntimeException("not connected");
            else if (round == 2) return Mono.error(new RuntimeException("timeout"));
            else return Mono.just(PingResponse.pong());
        });


        Duration wait = detector.pingInterval().multipliedBy(detector.livenessThreshold() * 2L);
        StepVerifier.withVirtualTime(() -> detector.watch(member))
                .expectSubscription()
                .expectNoEvent(wait)
                .thenCancel()
                .verify();

    }
}
