package io.odsp.subscription.controller.replication;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.subscription.Destination;
import io.odsp.subscription.commons.protocol.subscription.NamingPattern;
import io.odsp.subscription.commons.protocol.subscription.Source;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionSpec;
import io.odsp.subscription.controller.subscription.Subscription;
import io.odsp.subscription.controller.subscription.SubscriptionFactory;
import io.odsp.subscription.controller.subscription.SubscriptionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.ActiveProfiles;

import java.nio.charset.Charset;
import java.time.Instant;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;

@ActiveProfiles("docker")
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@EnableJpaAuditing
public class ReplicationBundleRepositoryTest {
    @Autowired
    private ReplicationBundleRepository repository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    private final SubscriptionSpec spec = new SubscriptionSpec(
            "app",
            Protocol.CEPH_FS, Format.JSON, Charset.defaultCharset().name(),
            Source.of("source"), NamingPattern.of("**"), TailorExpression.of(""), Destination.of("dest")
    );

    @Test
    public void queryStrandedSubscriptions() {
        //given a subscriptionSpec without related tasks
        Subscription subscription = new SubscriptionFactory(subscriptionRepository).build(spec);
        subscriptionRepository.putIfAbsent(subscription);
        subscription = subscriptionRepository.findActiveSubscription(subscription).get();

        //the current subscriptionSpec is a stranded active subscriptionSpec
        assertThat(repository.findActiveSubscriptionsHaveNotBeenStarted(Instant.now()), contains(subscription.sid()));

        //when tasks are created
        List<ReplicationBundle> bundles = repository.saveAll(new ReplicationBundleFactory().replicationsFor(subscription));
        //then preceding subscriptionSpec is not stranded
        assertThat(repository.findActiveSubscriptionsHaveNotBeenStarted(Instant.now()), empty());

        //tasks can be scheduled to one member.
        bundles.forEach(t -> t.onScheduled(new MemberId(Node.builder().host("h").port(1900).build(), Epoch.first())));
        repository.saveAll(bundles);

        //when the subscriptionSpec is deactivated
        subscriptionRepository.save(subscription.unsubscribe());
        //then it is considered stranded
        assertThat(repository.findInActiveSubscriptionsHaveNotBeenStopped(Instant.now()), contains(subscription.sid()));

        //and it's not stranded if all related tasks are deactivated
        bundles.forEach(ReplicationBundle::onDismissed);
        bundles.forEach(ReplicationBundle::onStopped);
        repository.saveAll(bundles);
        assertThat(repository.findInActiveSubscriptionsHaveNotBeenStopped(Instant.now()), empty());
    }
}
