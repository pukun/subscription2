CREATE SEQUENCE seq_replication_offset INCREMENT BY 1 MINVALUE 1 MAXVALUE 99999999 START WITH 1;
CREATE SEQUENCE seq_replication_failure INCREMENT BY 1 MINVALUE 1 MAXVALUE 99999999 START WITH 1;
CREATE TABLE t_replication_offset (
  id                 SERIAL NOT NULL, 
  subscription_id    int4 NOT NULL, 
  partition_sequence int2 NOT NULL, 
  watermark_bucket   varchar(63) NOT NULL, 
  watermark_time     timestamp NOT NULL, 
  watermark_file     varchar(255) NOT NULL, 
  last_modified_time timestamp NOT NULL, 
  version            int4 NOT NULL, 
  created_time       timestamp NOT NULL, 
  PRIMARY KEY (id));
CREATE TABLE t_replication_failure (
  id              SERIAL NOT NULL, 
  subscription_id int4 NOT NULL, 
  bucket          varchar(63), 
  "file"          varchar(255), 
  code            varchar(63) NOT NULL, 
  detail          varchar(1023) NOT NULL, 
  occurred_time   timestamp NOT NULL, 
  created_time    timestamp NOT NULL, 
  PRIMARY KEY (id));
CREATE UNIQUE INDEX idx_replication_offset_partiton 
  ON t_replication_offset (subscription_id, partition_sequence);
CREATE INDEX idx_replication_error_subscription 
  ON t_replication_failure (subscription_id);
