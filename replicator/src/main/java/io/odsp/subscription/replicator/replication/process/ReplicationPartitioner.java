package io.odsp.subscription.replicator.replication.process;

public record ReplicationPartitioner(int totalPartitions) {
    public int partition(ReplicationFile file) {
        return file.name().hashCode() % totalPartitions;
    }

}
