package io.odsp.subscription.replicator.replication.failure;

import io.odsp.subscription.commons.persistence.Immutable;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.replicator.replication.offset.SubscriptionIdConverter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;

import static javax.persistence.GenerationType.SEQUENCE;

@Accessors(fluent = true, chain = true)
@Setter
@Entity
@Table(name = "t_replication_failure")
public class ReplicationFailure extends Immutable {
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "fileReplicationFailure")
    @SequenceGenerator(name = "fileReplicationFailure", sequenceName = "seq_replication_failure", allocationSize = 1)
    private int id;

    @Column(name = "subscription_id", nullable = false, updatable = false)
    @Convert(converter = SubscriptionIdConverter.class)
    private SubscriptionId subscription;

    @Column(updatable = false)
    private String bucket;

    @Column(updatable = false)
    private String file;

    @Enumerated(EnumType.STRING)
    private ReplicationFailureCode code;

    @Column(nullable = false, updatable = false)
    private String error;

}
