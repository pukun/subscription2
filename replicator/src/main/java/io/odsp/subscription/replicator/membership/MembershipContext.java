package io.odsp.subscription.replicator.membership;

import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;

public record MembershipContext(
        Node localNode, WebClient client,
        Duration applyInterval, Duration failureThreshold
) {
    private static final Logger logger = LoggerFactory.getLogger("membership");

    public Mono<Membership> apply() {
        return apply(Long.MAX_VALUE);
    }

    Mono<Membership> apply(long retryTimes) {
        return Mono.delay(applyInterval)
                .flatMap(i -> join())
                .retry(retryTimes)
                .map(memberId -> new Membership(memberId, failureThreshold));
    }


    Mono<MemberId> join() {
        return client.post().uri(Endpoints.join())
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(localNode)
                .retrieve()
                .bodyToMono(MemberId.class)
                .doOnError(t -> logger.info("failed to join cluster, reason: {}", t.getMessage()));
    }
}
