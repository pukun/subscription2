package io.odsp.subscription.replicator.membership;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.bind.DefaultValue;

@ConstructorBinding
@ConfigurationProperties("worker.membership")
public class MembershipProperties {

    private final Cluster cluster;
    private final Apply apply;
    private final Failure failure;

    public record Cluster(String protocol, String host, int port) {
        public Cluster(@DefaultValue("http") String protocol, String host, @DefaultValue("80") int port) {
            this.protocol = protocol;
            this.host = host;
            this.port = port;
        }

        public String url() {
            return String.format("%s://%s:%d", protocol, host, port);
        }
    }

    /**
     * @param interval apply interval
     */
    public record Apply(int interval) {
        public Apply(@DefaultValue("10") int interval) {
            this.interval = interval;
        }
    }

    public record Failure(int threshold) {
        public Failure(@DefaultValue("30") int threshold) {
            this.threshold = threshold;
        }


    }

    public MembershipProperties(Cluster cluster, Apply apply, Failure failure) {
        this.cluster = cluster;
        this.apply = apply;
        this.failure = failure;
    }

    public Apply getApply() {
        return apply;
    }

    public Failure getFailure() {
        return failure;
    }


    public Cluster getCluster() {
        return cluster;
    }
}
