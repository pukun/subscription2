package io.odsp.subscription.replicator.replication.offset;

import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Convert;
import javax.persistence.Embeddable;

@Accessors(fluent = true)
@Getter
@Setter
@Embeddable
@EqualsAndHashCode
public class ReplicationPartition {
    @Convert(attributeName = "subscription", converter = SubscriptionIdConverter.class)
    private SubscriptionId subscription;

    private int sequence;

    public static ReplicationPartition of(SubscriptionId subscription, int sequence) {
        return new ReplicationPartition()
                .subscription(subscription)
                .sequence(sequence);
    }
}
