package io.odsp.subscription.replicator.engine;


import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.Error;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import io.odsp.subscription.commons.protocol.replication.ReplicationResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.reactive.result.method.RequestMappingInfo;
import org.springframework.web.reactive.result.method.annotation.RequestMappingHandlerMapping;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

@RestController
@RequestMapping(Endpoints.MEMBER)
public class ReplicationEngineEndpoint {
    private static final Logger logger = LoggerFactory.getLogger("engine");

    private final ReplicationEngine engine;

    @Autowired
    public ReplicationEngineEndpoint(ReplicationEngine engine) {
        this.engine = engine;
    }

    @GetMapping(Endpoints.NODE)
    public Mono<Node> node() {
        return Mono.just(engine.identity());
    }

    @GetMapping(Endpoints.EPOCH)
    public Mono<Epoch> epoch() {
        return engine.memberId()
                .map(id -> id.map(MemberId::epoch))
                .map(epoch -> {
                    if (epoch.isPresent()) return epoch.get();
                    else throw new NoMembershipException();
                });

    }

    @PostMapping(Endpoints.PING)
    public Mono<PingResponse> ping(@PathVariable("epoch") int epoch) {
        return Mono.just(engine.ping(Epoch.of(epoch)));
    }


    @PostMapping(Endpoints.REPLICATE)
    public Flux<ReplicationResponse> invoke(@PathVariable("epoch") int epoch, @RequestBody Flux<ReplicationRequest> requests) {
        try {
            return requests.collectList()
                    .flatMap(commands -> engine.invoke(Epoch.of(epoch), commands))
                    .flatMapIterable(responses -> responses)
                    .onErrorMap(InvocationException::new);
        } catch (Throwable t) {
            logger.info("Unexpected exception raised when invoke commands", t);
            throw new InvocationException(t);
        }
    }

    static class NoMembershipException extends RuntimeException {
    }

    public static class InvocationException extends RuntimeException {
        InvocationException(Throwable t) {
            super(t);
        }
    }

    @ExceptionHandler
    public ResponseEntity<Error> onError(NoMembershipException t) {
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler
    public ResponseEntity<Error> onError(InvocationException t) {
        return ResponseEntity.unprocessableEntity().body(new Error("invocation", t.getCause().getMessage()));
    }

    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        ApplicationContext applicationContext = event.getApplicationContext();
        RequestMappingHandlerMapping requestMappingHandlerMapping = applicationContext
                .getBean("requestMappingHandlerMapping", RequestMappingHandlerMapping.class);
        Map<RequestMappingInfo, HandlerMethod> map = requestMappingHandlerMapping
                .getHandlerMethods();
        map.forEach((key, value) -> logger.info("{} {}", key, value));
    }
}
