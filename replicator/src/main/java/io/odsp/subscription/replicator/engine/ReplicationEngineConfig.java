package io.odsp.subscription.replicator.engine;

import io.odsp.subscription.replicator.membership.MembershipContext;
import io.odsp.subscription.replicator.replication.ReplicationProcessorContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReplicationEngineConfig {

    @Bean
    public ReplicationEngineContext engineContext(MembershipContext membershipContext, ReplicationProcessorContext processContext) {
        return new ReplicationEngineContext(membershipContext, processContext);
    }

    @Bean
    public ReplicationEngine engine(ReplicationEngineContext context) {
        return ReplicationEngine.start(context);
    }
}
