package io.odsp.subscription.replicator.replication.offset;

import io.odsp.subscription.commons.protocol.replication.Replication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ReplicationOffsetRepository extends JpaRepository<ReplicationOffset, Integer> {
    @Query("""
            select offset from ReplicationOffset offset where
            offset.partition.subscription = :#{#replication.subscription.id}
            and offset.partition.sequence >= :#{#replication.from}
            and offset.partition.sequence <= :#{#replication.to}
            """
    )
    List<ReplicationOffset> offsetOf(@Param("replication") Replication replication);

    @Query("""
            update ReplicationOffset set watermark = :watermark
            where partition = :partition
            """
    )
    @Modifying
    int set(@Param("partition") ReplicationPartition partition, @Param("watermark") ReplicationWatermark watermark);

    Optional<ReplicationOffset> findByPartition(ReplicationPartition partition);
}
