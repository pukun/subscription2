package io.odsp.subscription.replicator.replication.process;

import io.odsp.subscription.replicator.replication.offset.ReplicationWatermark;

import java.nio.file.PathMatcher;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public record ReplicationFilter(
        PathMatcher matcher, ReplicationPartitioner partitioner, Map<Integer, ReplicationWatermark> watermarks
) implements Predicate<ReplicationFile> {

    @Override
    public boolean test(ReplicationFile file) {
        // not match naming pattern
        if (!matcher.matches(file.path())) return false;

        // should in specified partitions and after related watermark
        return Optional.ofNullable(watermarks.get(partitionOf(file)))
                .map(watermark -> file.watermark().isAfter(watermark))
                .orElse(false);
    }

    public int partitionOf(ReplicationFile file) {
        return partitioner.partition(file);
    }
}
