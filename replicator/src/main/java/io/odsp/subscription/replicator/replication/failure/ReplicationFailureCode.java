package io.odsp.subscription.replicator.replication.failure;

public enum ReplicationFailureCode {
    NotAccessible("can not access file"),
    TailorException("failed to tailor"),
    DumpFailure("can not copy file to destination"),
    UnknownException("unexpected error");

    private final String description;

    ReplicationFailureCode(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
