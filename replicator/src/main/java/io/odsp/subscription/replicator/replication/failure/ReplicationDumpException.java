package io.odsp.subscription.replicator.replication.failure;

public class ReplicationDumpException extends RuntimeException {
    public ReplicationDumpException(String message, Throwable t) {
        super(message, t);
    }
}
