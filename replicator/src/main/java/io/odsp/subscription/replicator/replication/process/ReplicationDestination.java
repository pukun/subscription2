package io.odsp.subscription.replicator.replication.process;

import io.odsp.subscription.replicator.replication.failure.ReplicationAccessException;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

public record ReplicationDestination(Path destination) {

    public void dump(ReplicationFile file, String content, Charset encoding) {
        createBucketIfNeeded(file);
        write(file, content, encoding);
    }

    void createBucketIfNeeded(ReplicationFile file) {
        try {
            Files.createDirectories(pathOfBucket(file));
        } catch (Throwable e) {
            String message = String.format("Failed to create bucket '%s' in destination '%s'", file.bucket(), destination);
            throw new ReplicationAccessException(message, e);
        }
    }

    void write(ReplicationFile file, String content, Charset encoding) {
        try {
            Files.writeString(
                    pathOfFile(file),
                    content, encoding,
                    StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING
            );
        } catch (Throwable e) {
            String message = String.format("Failed to dump content of file '%s' into bucket '%s' at destination '%s' ", file.name(), file.bucket(), destination);
            throw new ReplicationAccessException(message, e);
        }
    }

    Path pathOfBucket(ReplicationFile file) {
        return destination.resolve(file.bucket());
    }

    Path pathOfFile(ReplicationFile file) {
        return pathOfBucket(file).resolve(file.name());
    }
}
