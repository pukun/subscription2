package io.odsp.subscription.replicator.replication.process;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.JSONTailor;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.content.XMLTailor;

public record ReplicationTailor(JSONTailor jsonTailor, XMLTailor xmlTailor) {
    public String tailor(String content, Format format, TailorExpression expression) {
        return switch (format) {
            case XML -> xmlTailor.tailor(content, expression.exp());
            case JSON -> jsonTailor.tailor(content, expression.exp());
        };
    }
}
