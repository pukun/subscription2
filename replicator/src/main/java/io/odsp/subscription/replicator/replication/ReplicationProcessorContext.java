package io.odsp.subscription.replicator.replication;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.protocol.replication.Replication;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionSpec;
import io.odsp.subscription.replicator.replication.process.*;
import io.odsp.subscription.replicator.replication.failure.*;
import io.odsp.subscription.replicator.replication.metric.ReplicationMeter;
import io.odsp.subscription.replicator.replication.offset.ReplicationOffset;
import io.odsp.subscription.replicator.replication.offset.ReplicationOffsetRepository;
import io.odsp.subscription.replicator.replication.offset.ReplicationPartition;
import io.odsp.subscription.replicator.replication.offset.ReplicationWatermark;

import java.nio.charset.Charset;
import java.nio.file.Path;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.stream.Stream;

import static java.time.Instant.now;

public record ReplicationProcessorContext(
        int batchSize,
        ReplicationPartitioner partitioner,
        ReplicationTailor tailor,
        ReplicationOffsetRepository offsetRepository,
        ReplicationFailureRepository failureRepository,
        ReplicationMeter replicationMeter
) {
    List<ReplicationOffset> offsets(Replication bundle) {
        return offsetRepository().offsetOf(bundle);
    }

    List<ReplicationBucket> list(SubscriptionSpec subscription, Stream<ReplicationWatermark> watermarks) {
        Path source = Path.of(subscription.source().path());
        return new ReplicationSource(source).ls(watermarks);
    }

    List<ReplicationFile> scan(ReplicationBucket bucket, ReplicationFilter filter, int limit) {
        return bucket.scan(filter, limit);
    }

    void tailorAndThenDump(SubscriptionSpec subscription, ReplicationFile file) {
        Charset encoding = Charset.forName(subscription.encoding());
        Format format = subscription.format();
        TailorExpression expression = subscription.tailor();
        Path destination = Path.of(subscription.destination().path());

        new ReplicationDestination(destination).dump(file, file.tailor(encoding, format, expression, tailor), encoding);
    }

    void abort(SubscriptionId subscriptionId, Throwable t) {
        failureRepository().save(failureOf(subscriptionId, t));
    }

    void abort(SubscriptionId subscriptionId, ReplicationFile file, Throwable t) {
        failureRepository().save(failureOf(subscriptionId, t).bucket(file.bucket()).file(file.name()));
    }

    private ReplicationFailure failureOf(SubscriptionId subscriptionId, Throwable t) {
        ReplicationFailure failure = new ReplicationFailure().subscription(subscriptionId);

        if (t instanceof ReplicationAccessException ae)
            return failure.code(ReplicationFailureCode.NotAccessible).error(t.getCause().getMessage());
        else if (t instanceof ReplicationDumpException)
            return failure.code(ReplicationFailureCode.DumpFailure).error(t.getCause().getMessage());
        else if (t instanceof ReplicationTailorException)
            return failure.code(ReplicationFailureCode.TailorException).error(t.getCause().getMessage());
        else return failure.code(ReplicationFailureCode.UnknownException).error(t.getMessage());
    }


    void commit(Instant start, SubscriptionId subscriptionId, ReplicationFile file, ReplicationPartitioner partitioner) {
        offsetRepository.set(ReplicationPartition.of(subscriptionId, partitioner.partition(file)), file.watermark());
        replicationMeter.record(subscriptionId, Duration.between(start, now()));
    }

}