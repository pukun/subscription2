package io.odsp.subscription.replicator.replication.process;

import io.odsp.subscription.replicator.replication.failure.ReplicationAccessException;
import io.odsp.subscription.replicator.replication.offset.ReplicationWatermark;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public record ReplicationSource(Path source) {

    public List<ReplicationBucket> ls(Stream<ReplicationWatermark> watermarks) {
        return ls(bucketWatermark(watermarks));
    }

    private String bucketWatermark(Stream<ReplicationWatermark> watermarks) {
        return watermarks
                .min(ReplicationWatermark::compareTo)
                .map(ReplicationWatermark::bucket)
                .orElse("");
    }

    private List<ReplicationBucket> ls(String bucketWatermark) {
        try (Stream<Path> paths = Files.list(source)) {
            return paths
                    .filter(Files::isDirectory)
                    .map(this::bucketFrom)
                    .sorted(Comparator.comparing(ReplicationBucket::name))
                    .dropWhile(bucket -> bucket.name().compareTo(bucketWatermark) < 0)
                    .collect(Collectors.toList());
        } catch (IOException | SecurityException t) {
            throw new ReplicationAccessException(String.format("can not list buckets in source '%s'", source), t);
        }
    }

    private ReplicationBucket bucketFrom(Path file) {
        try {
            return new ReplicationBucket(file, Files.getLastModifiedTime(file));
        } catch (IOException | SecurityException e) {
            throw new ReplicationAccessException(String.format("failed to create bucket from file '%s'", file), e);
        }
    }
}
