package io.odsp.subscription.replicator.replication.process;

import io.odsp.subscription.replicator.replication.failure.ReplicationAccessException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;

public record ReplicationBucket(Path path, FileTime lastModifiedTime) {

    public String name() {
        return path.getFileName().toString();
    }

    public List<ReplicationFile> scan(ReplicationFilter filter, int limit) {
        try (Stream<Path> files = Files.list(path)) {
            return files.filter(Predicate.not(Files::isDirectory))
                    .map(this::fileOf)
                    .sorted()
                    .dropWhile(Predicate.not(filter))
                    .limit(limit)
                    .toList();
        } catch (IOException | SecurityException e) {
            throw new ReplicationAccessException(String.format("Failed to list files in bucket(%s) ", name()), e);
        } catch (ReplicationAccessException t) {
            throw t;
        }
    }

    private ReplicationFile fileOf(Path file) {
        try {
            return new ReplicationFile(name(), file, Files.getLastModifiedTime(file));
        } catch (IOException | SecurityException t) {
            String message = String.format("Failed to get 'lastModifiedTime' of file(%s) in bucket(%s) ", file.getFileName().toString(), name());
            throw new ReplicationAccessException(message, t);
        }
    }
}
