package io.odsp.subscription.replicator.replication.failure;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ReplicationFailureRepository extends JpaRepository<ReplicationFailure, Integer> {
}
