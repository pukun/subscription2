package io.odsp.subscription.replicator.replication.metric;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;

import java.time.Duration;

public record ReplicationMeter(MeterRegistry registry) {
    public static final String FILE_REPLICATION = "file.subscription";

    public void record(SubscriptionId subscription, Duration replicationDuration) {
        registry.timer(FILE_REPLICATION, tagsOf(subscription))
                .record(replicationDuration);
    }

    Tags tagsOf(SubscriptionId subscription) {
        return Tags.of(Tag.of("subscription", Integer.toString(subscription.id())));
    }
}
