package io.odsp.subscription.replicator.membership;

import io.odsp.subscription.commons.protocol.membership.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.time.Duration;

@Configuration
@EnableConfigurationProperties(MembershipProperties.class)
public class MembershipConfig {
    private static final Logger logger = LoggerFactory.getLogger("membership");

    @Bean
    public MembershipContext membershipContext(@Value("${server.port}") int port, MembershipProperties properties) throws IOException {
        MembershipProperties.Cluster cluster = properties.getCluster();
        logger.info("Cluster setting: {}", cluster);

        MembershipContext context = new MembershipContext(
                localNode(cluster, port),
                clientWith(cluster),
                Duration.ofSeconds(properties.getApply().interval()),
                Duration.ofSeconds(properties.getFailure().threshold())
        );
        logger.info("Initialized context[node: {}, applyInterval: {}, failureThreshold: {}]",
                context.localNode(), context.applyInterval(), context.failureThreshold());

        return context;
    }

    Node localNode(MembershipProperties.Cluster cluster, int nodePort) throws IOException {
        try (final DatagramSocket socket = new DatagramSocket()) {
            socket.connect(new InetSocketAddress("8.8.8.8", cluster.port()));
            String nodeHost = socket.getLocalAddress().getHostAddress();
            return Node.builder().host(nodeHost).port(nodePort).build();
        }
    }

    WebClient clientWith(MembershipProperties.Cluster cluster) {
        return clientWith(cluster.url());
    }

    WebClient clientWith(String baseUrl) {
        HttpClient httpClient = HttpClient.create()
                .responseTimeout(Duration.ofSeconds(3));

        return WebClient.builder()
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .baseUrl(baseUrl)
                .build();
    }
}
