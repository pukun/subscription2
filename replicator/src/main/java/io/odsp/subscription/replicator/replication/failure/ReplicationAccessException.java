package io.odsp.subscription.replicator.replication.failure;

public class ReplicationAccessException extends RuntimeException {
    public ReplicationAccessException(String message, Throwable t) {
        super(message, t);
    }
}
