package io.odsp.subscription.replicator.replication.offset;

import io.odsp.subscription.commons.persistence.Mutable;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Objects;

import static javax.persistence.GenerationType.SEQUENCE;

@Accessors(fluent = true, chain = true)
@Getter
@Setter
@Entity
@Table(name = "t_replication_offset")
public class ReplicationOffset extends Mutable {
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "fileReplicationOffset")
    @SequenceGenerator(name = "fileReplicationOffset", sequenceName = "seq_replication_offset", allocationSize = 1)
    private Integer id;

    @Embedded()
    @AttributeOverrides({
            @AttributeOverride(name = "subscription", column = @Column(name = "subscription_id", nullable = false)),
            @AttributeOverride(name = "sequence", column = @Column(name = "partition_sequence", nullable = false))
    })
    private ReplicationPartition partition;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "bucket", column = @Column(name = "watermark_bucket", nullable = false)),
            @AttributeOverride(name = "time", column = @Column(name = "watermark_time", nullable = false)),
            @AttributeOverride(name = "file", column = @Column(name = "watermark_file", nullable = false))
    })
    private ReplicationWatermark watermark;


    public static ReplicationOffset initialOffsetOf(ReplicationPartition partition) {
        return new ReplicationOffset()
                .partition(partition)
                .watermark(ReplicationWatermark.epoch());
    }

    public static ReplicationOffset of(ReplicationPartition p, ReplicationWatermark w) {
        return new ReplicationOffset().partition(p).watermark(w);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReplicationOffset that)) return false;
        return Objects.equals(partition, that.partition) && Objects.equals(watermark, that.watermark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(partition, watermark);
    }
}
