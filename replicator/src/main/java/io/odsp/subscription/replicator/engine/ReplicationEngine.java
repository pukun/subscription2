package io.odsp.subscription.replicator.engine;

import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import io.odsp.subscription.commons.protocol.replication.Replication;
import io.odsp.subscription.commons.protocol.replication.ReplicationId;
import io.odsp.subscription.commons.protocol.replication.ReplicationRequest;
import io.odsp.subscription.commons.protocol.replication.ReplicationResponse;
import io.odsp.subscription.replicator.membership.Membership;
import io.odsp.subscription.replicator.replication.ReplicationProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Stream;

public class ReplicationEngine {
    private static final Logger logger = LoggerFactory.getLogger("engine");


    //region context
    private final ScheduledExecutorService actor = Executors.newSingleThreadScheduledExecutor(r -> new Thread(r, "replicationEngine"));
    private final ReplicationEngineContext context;
    private final ForkJoinPool executor = new ForkJoinPool();
    //endregion


    //region status
    private volatile Optional<Membership> membership;
    private final Map<ReplicationId, ReplicationProcessor> processors = new HashMap<>();
    private final Map<ReplicationId, ScheduledFuture<?>> processes = new HashMap<>();
    //endregion


    //region constructor
    private ReplicationEngine(ReplicationEngineContext context) {
        this.context = context;
        this.membership = Optional.empty();
    }

    public static ReplicationEngine start(ReplicationEngineContext context) {
        ReplicationEngine engine = new ReplicationEngine(context);

        engine.applyForMembership();

        return engine;
    }
    //endregion


    //region membership
    Node identity() {
        return context.membershipContext().localNode();
    }

    void applyForMembership() {
        context.applyForMembership()
                .doOnError(t -> logger.error("Exception raised when applying membership", t))
                .subscribeOn(Schedulers.fromExecutorService(actor))
                .subscribe(this::onMembershipAttained);
    }

    void onMembershipAttained(Membership membership) {
        logger.info("Has attained membership {} ", membership);

        this.membership = Optional.of(membership);

        membership.watch()
                .doOnError(t -> logger.error("Exception raised when watching membership", t))
                .subscribeOn(Schedulers.fromExecutorService(actor))
                .subscribe(v -> {
                }, t -> {
                }, this::onMembershipLost);
    }

    void onMembershipLost() {
        membership.ifPresent(m -> logger.info("Membership {} has lost", m));
        membership = Optional.empty();

        halt();

        applyForMembership();
    }

    PingResponse ping(Epoch epoch) {
        return membership.map(m -> {
                    if (m.memberId().epoch().equals(epoch)) {
                        m.onPinged();
                        return PingResponse.pong();
                    } else return PingResponse.mismatch();
                })
                .orElse(PingResponse.noMembership());
    }

    Mono<Optional<MemberId>> memberId() {
        return Mono.create(sink -> actor.submit(() -> sink.success(
                membership.map(Membership::memberId)
        )));
    }

    boolean isInCluster(Epoch epoch) {
        return membership.map(Membership::memberId)
                .map(MemberId::epoch)
                .filter(e -> e.equals(epoch))
                .isPresent();
    }
    //endregion


    //region replication processing
    public Mono<List<ReplicationResponse>> invoke(Epoch epoch, List<ReplicationRequest> requests) {
        return Mono.create(sink -> actor.submit(() -> {
            if (isInCluster(epoch)) sink.success(onInvokeRequest(requests));
            else sink.error(new RuntimeException("no membership"));
        }));
    }

    List<ReplicationResponse> onInvokeRequest(List<ReplicationRequest> requests) {
        Stream<ReplicationResponse> stopped = requests.stream()
                .filter(request -> request instanceof ReplicationRequest.Stop)
                .map(request -> (ReplicationRequest.Stop) request)
                .map(this::invoke);

        Stream<ReplicationResponse> started = requests.stream()
                .filter(request -> request instanceof ReplicationRequest.Start)
                .map(request -> (ReplicationRequest.Start) request)
                .map(this::invoke);

        return Stream.concat(stopped, started).toList();
    }

    void halt() {
        processors.clear();
        processes.values().forEach(f -> f.cancel(false));
        processes.clear();
        logger.info("All processors have been stopped due to disconnection with controller");
    }

    ReplicationResponse invoke(ReplicationRequest.Stop request) {
        processors.remove(request.replicationId());
        Optional.ofNullable(processes.remove(request.replicationId())).ifPresent(f -> f.cancel(false));
        return ReplicationResponse.stop(request.replicationId());
    }

    ReplicationResponse invoke(ReplicationRequest.Start request) {
        Replication replication = request.replication();

        ReplicationProcessor processor = new ReplicationProcessor(replication, context.processContext());
        processors.put(replication.id(), processor);
        processes.put(replication.id(), actor.scheduleAtFixedRate(() -> replicate(replication.id()), 30, 10, TimeUnit.SECONDS));

        return ReplicationResponse.start(replication.id());
    }

    private void replicate(ReplicationId replicationId) {
        Optional.ofNullable(processors.get(replicationId))
                .ifPresent(processor -> executor.execute(processor::replicate));
    }

    //endregion

}
