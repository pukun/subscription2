package io.odsp.subscription.replicator.replication.offset;

import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class SubscriptionIdConverter implements AttributeConverter<SubscriptionId, Integer> {
    @Override
    public Integer convertToDatabaseColumn(SubscriptionId attribute) {
        return attribute.id();
    }

    @Override
    public SubscriptionId convertToEntityAttribute(Integer dbData) {
        return new SubscriptionId(dbData);
    }
}
