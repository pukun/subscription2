package io.odsp.subscription.replicator.engine;

import io.odsp.subscription.replicator.membership.Membership;
import io.odsp.subscription.replicator.membership.MembershipContext;
import io.odsp.subscription.replicator.replication.ReplicationProcessorContext;
import reactor.core.publisher.Mono;

public record ReplicationEngineContext(
        MembershipContext membershipContext,
        ReplicationProcessorContext processContext
) {
    Mono<Membership> applyForMembership() {
        return membershipContext.apply();
    }
}

