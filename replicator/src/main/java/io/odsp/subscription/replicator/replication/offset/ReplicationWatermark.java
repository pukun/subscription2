package io.odsp.subscription.replicator.replication.offset;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Embeddable;
import java.time.Instant;

@Accessors(fluent = true)
@Getter
@Setter
@EqualsAndHashCode
@Embeddable
public class ReplicationWatermark implements Comparable<ReplicationWatermark> {
    private String bucket;
    private Instant time;
    private String file;

    public static ReplicationWatermark epoch() {
        return new ReplicationWatermark()
                .bucket("")
                .file("")
                .time(Instant.EPOCH);
    }

    public static ReplicationWatermark of(String bucket, Instant time, String name) {
        return new ReplicationWatermark().bucket(bucket).time(time).file(name);
    }


    public boolean isAfter(ReplicationWatermark o) {
        return compareTo(o) > 0;
    }

    @Override
    public int compareTo(ReplicationWatermark o) {
        if (bucket.equals(o.bucket)) return compareFile(o);
        else return bucket.compareTo(o.bucket);
    }

    private int compareFile(ReplicationWatermark o) {
        if (time.equals(o.time)) return file.compareTo(o.file);
        else return time.compareTo(o.time);
    }
}
