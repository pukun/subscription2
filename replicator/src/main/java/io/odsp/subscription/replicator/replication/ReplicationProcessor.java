package io.odsp.subscription.replicator.replication;

import io.odsp.subscription.commons.protocol.replication.Replication;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionSpec;
import io.odsp.subscription.replicator.replication.process.ReplicationBucket;
import io.odsp.subscription.replicator.replication.process.ReplicationFile;
import io.odsp.subscription.replicator.replication.process.ReplicationFilter;
import io.odsp.subscription.replicator.replication.process.ReplicationPartitioner;
import io.odsp.subscription.replicator.replication.failure.ReplicationTailorException;
import io.odsp.subscription.replicator.replication.offset.ReplicationOffset;
import io.odsp.subscription.replicator.replication.offset.ReplicationWatermark;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.FileSystems;
import java.nio.file.PathMatcher;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public record ReplicationProcessor(Replication replication, ReplicationProcessorContext context) {

    private static final Logger logger = LoggerFactory.getLogger("replication");


    public void replicate() {
        logger.info("start to replicate for '{}' ", replication.id());
        try {
            replicate(context.offsets(replication));
            logger.info("replication for '{}' is finished", replication.id());
        } catch (Throwable t) {
            logger.info("replication for '{}' is aborted", replication.id(), t);
        }
    }

    void replicate(List<ReplicationOffset> offsets) {
        ReplicationFilter filter = new ReplicationFilter(pathMatcher(), partitioner(), watermarks(offsets));

        List<ReplicationBucket> buckets;
        try {
            buckets = context.list(spec(), filter.watermarks().values().stream());
        } catch (Throwable t) {
            context.abort(subscriptionId(), t);
            throw t;
        }

        int filesToReplicate = context.batchSize();
        for (ReplicationBucket bucket : buckets) {
            filesToReplicate -= replicate(bucket, filter, filesToReplicate);
            if (filesToReplicate == 0) break;
        }
    }


    int replicate(ReplicationBucket bucket, ReplicationFilter filter, int limit) {
        List<ReplicationFile> files;

        try {
            files = context.scan(bucket, filter, limit);
        } catch (Throwable t) {
            context.abort(subscriptionId(), t);
            throw t;
        }

        int replicated = 0;
        for (ReplicationFile file : files) {
            if (replicate(file, filter.partitioner())) {
                replicated++;
            }
        }

        return replicated;
    }

    boolean replicate(ReplicationFile file, ReplicationPartitioner partitioner) {
        try {
            Instant now = Instant.now();
            context.tailorAndThenDump(spec(), file);
            context.commit(now, subscriptionId(), file, partitioner);
            return true;
        } catch (ReplicationTailorException te) {
            context.abort(subscriptionId(), file, te);
            return false;
        } catch (Throwable t) {
            context.abort(subscriptionId(), file, t);
            throw t;
        }
    }

    Map<Integer, ReplicationWatermark> watermarks(List<ReplicationOffset> offsets) {
        Map<Integer, ReplicationWatermark> watermarks = offsets.stream().collect(Collectors.toMap(
                offset -> offset.partition().sequence(), ReplicationOffset::watermark));

        IntStream.rangeClosed(replication.from(), replication.to())
                .boxed()
                .forEach(partition -> watermarks.putIfAbsent(partition, ReplicationWatermark.epoch()));

        return watermarks;
    }

    PathMatcher pathMatcher() {
        return FileSystems.getDefault().getPathMatcher(spec().filter().pattern());
    }

    ReplicationPartitioner partitioner() {
        return new ReplicationPartitioner(replication.total());
    }

    SubscriptionId subscriptionId() {
        return replication.subscription().id();
    }

    SubscriptionSpec spec() {
        return replication.subscription().spec();
    }

}