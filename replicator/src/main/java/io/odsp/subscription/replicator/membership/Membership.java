package io.odsp.subscription.replicator.membership;

import io.odsp.subscription.commons.protocol.membership.MemberId;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

@Accessors(fluent = true)
@Getter
public class Membership {
    private static final Logger logger = LoggerFactory.getLogger("membership");

    private final MemberId memberId;
    private final Duration failureThreshold;

    private Instant instantBeingPinged;

    Membership(MemberId memberId, Duration failureThreshold) {
        this(memberId, failureThreshold, Instant.now());
    }

    Membership(MemberId memberId, Duration failureThreshold, Instant now) {
        this.memberId = memberId;
        this.failureThreshold = failureThreshold;

        this.instantBeingPinged = now;
    }


    public void onPinged() {
        instantBeingPinged = Instant.now();
    }

    public Mono<Void> watch() {
        return watch(Instant.now());
    }

    Mono<Void> watch(Instant checkpoint) {
        return Flux.interval(failureThreshold)
                .filter(i -> isExpired(checkpoint.plus(failureThreshold.multipliedBy(i))))
                .next()
                .then();
    }

    boolean isExpired(Instant checkpoint) {
        return instantBeingPinged.isBefore(checkpoint);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Membership)) return false;
        Membership that = (Membership) o;
        return failureThreshold == that.failureThreshold && memberId.equals(that.memberId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberId, failureThreshold);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("memberId", memberId)
                .toString();
    }
}
