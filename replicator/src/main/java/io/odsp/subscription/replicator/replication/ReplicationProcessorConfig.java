package io.odsp.subscription.replicator.replication;

import io.micrometer.core.instrument.MeterRegistry;
import io.odsp.subscription.commons.content.JSONTailor;
import io.odsp.subscription.commons.content.XMLTailor;
import io.odsp.subscription.commons.protocol.replication.Replication;
import io.odsp.subscription.replicator.replication.process.ReplicationPartitioner;
import io.odsp.subscription.replicator.replication.process.ReplicationTailor;
import io.odsp.subscription.replicator.replication.failure.ReplicationFailureRepository;
import io.odsp.subscription.replicator.replication.metric.ReplicationMeter;
import io.odsp.subscription.replicator.replication.offset.ReplicationOffsetRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ReplicationProcessorConfig {

    @Value("${replication.batch.size:100}")
    private int batchSize;


    @Bean
    public ReplicationProcessorContext processorConfig(
            ReplicationOffsetRepository offsetRepository,
            ReplicationFailureRepository failureRepository,
            MeterRegistry meterRegistry
    ) {
        return new ReplicationProcessorContext(
                batchSize,
                new ReplicationPartitioner(Replication.TOTAL_PARTITIONS),
                new ReplicationTailor(new JSONTailor(), new XMLTailor()),
                offsetRepository,
                failureRepository,
                new ReplicationMeter(meterRegistry)
        );
    }
}