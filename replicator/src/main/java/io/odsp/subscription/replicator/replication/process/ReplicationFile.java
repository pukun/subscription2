package io.odsp.subscription.replicator.replication.process;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.replicator.replication.failure.ReplicationAccessException;
import io.odsp.subscription.replicator.replication.failure.ReplicationTailorException;
import io.odsp.subscription.replicator.replication.offset.ReplicationWatermark;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;

public record ReplicationFile(
        String bucket, Path path, FileTime lastModifiedTime
) implements Comparable<ReplicationFile> {

    public String name() {
        return path.getFileName().toString();
    }

    public ReplicationWatermark watermark() {
        return new ReplicationWatermark().time(lastModifiedTime().toInstant()).file(name());
    }


    public String tailor(Charset encoding, Format format, TailorExpression expression, ReplicationTailor tailor) {
        return tailor(read(encoding), format, expression, tailor);
    }

    String read(Charset encoding) {
        try {
            return Files.readString(path, encoding);
        } catch (IOException | SecurityException t) {
            String message = String.format("Failed to read content of file '%s' in bucket '%s'", name(), bucket());
            throw new ReplicationAccessException(message, t);
        }
    }

    String tailor(String content, Format format, TailorExpression expression, ReplicationTailor tailor) {
        try {
            return tailor.tailor(content, format, expression);
        } catch (Throwable t) {
            String message = String.format("Failed to tailor file '%s' by expression '%s'", name(), expression.exp());
            throw new ReplicationTailorException(message, t);
        }
    }

    @Override
    public int compareTo(ReplicationFile o) {
        if (lastModifiedTime.equals(o.lastModifiedTime)) return path.compareTo(o.path);
        else return lastModifiedTime.compareTo(o.lastModifiedTime);
    }
}
