package io.odsp.subscription.replicator.replication.failure;

public class ReplicationTailorException extends RuntimeException {
    public ReplicationTailorException(String message, Throwable t) {
        super(message, t);
    }
}
