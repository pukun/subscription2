package io.odsp.subscription.replicator.engine;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.Protocol;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.PingResponse;
import io.odsp.subscription.commons.protocol.replication.*;
import io.odsp.subscription.commons.protocol.subscription.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;

@WebFluxTest(ReplicationEngineEndpoint.class)
public class ReplicationEngineEndpointTest {
    @Autowired
    private WebTestClient client;

    @MockBean
    private ReplicationEngine engine;

    private final Epoch epoch = Epoch.first();
    private final ReplicationId activeTaskId = ReplicationId.of(22);
    private final ReplicationId dormantTaskId = ReplicationId.of(23);

    private final SubscriptionSpec spec = new SubscriptionSpec(
            "app", Protocol.CEPH_FS,
            Format.XML, Charset.defaultCharset().name(),
            Source.of("source"), new NamingPattern("pattern"),
            TailorExpression.of("tailor"),
            Destination.of("destination")
    );

    private final Replication replication = new Replication(
            activeTaskId,
            new Subscription(SubscriptionId.of(100), spec),
            100, 10, 20
    );

    private final ReplicationRequest.Start startRequest = ReplicationRequest.start(replication);
    private final ReplicationRequest.Stop stopRequest = ReplicationRequest.stop(dormantTaskId);
    private final List<ReplicationRequest> requestList = Arrays.asList(startRequest, stopRequest);

    @Nested
    @DisplayName("ping request process")
    class PingTests {
        @Test
        @DisplayName("engine could be pinged")
        public void ping() {
            given(engine.ping(epoch)).willReturn(PingResponse.mismatch());

            client.post().uri(Endpoints.ping(epoch))
                    .accept(MediaType.APPLICATION_JSON)
                    .exchange()
                    .expectBody(PingResponse.class)
                    .isEqualTo(PingResponse.mismatch());

        }

        @Test
        @DisplayName(" epoch notFound when not in cluster")
        public void epoch_not_found_when_not_in_cluster() {
            given(engine.memberId()).willReturn(Mono.just(Optional.empty()));

            client.get().uri(Endpoints.epoch())
                    .exchange()
                    .expectStatus().isNotFound();
        }

    }

    @Nested
    @DisplayName("invocation request process")
    class InvocationTests {
        @Test
        @DisplayName("engin accepts invocation requests")
        public void given_engine_accept_invocation() {
            given(engine.invoke(epoch, requestList))
                    .willReturn(Mono.just(Arrays.asList(
                            ReplicationResponse.stop(activeTaskId),
                            ReplicationResponse.reject(dormantTaskId, "test", "test")
                    )));

            invoke()
                    .expectStatus().isOk()
                    .expectBodyList(ReplicationResponse.class)
                    .contains(
                            ReplicationResponse.stop(activeTaskId),
                            ReplicationResponse.reject(dormantTaskId, "test", "test")
                    );
        }

        @Test
        @DisplayName("engine refuses invocation requests")
        public void given_engine_refuses_invocation() {
            given(engine.invoke(epoch, requestList))
                    .willReturn(Mono.error(new RuntimeException("no membership")));

            WebTestClient.BodyContentSpec startResponse = invoke()
                    .expectStatus().isEqualTo(HttpStatus.UNPROCESSABLE_ENTITY)
                    .expectBody();

            startResponse
                    .jsonPath("$.code").isEqualTo("invocation")
                    .jsonPath("$.message").isEqualTo("no membership");
        }

        private WebTestClient.ResponseSpec invoke() {
            return client
                    .post().uri(Endpoints.replicate(epoch))
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(Flux.fromIterable(requestList), ReplicationRequest.class)
                    .exchange();
        }
    }


}
