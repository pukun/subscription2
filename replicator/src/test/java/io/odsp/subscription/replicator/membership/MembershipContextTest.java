package io.odsp.subscription.replicator.membership;

import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.time.Duration;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

public class MembershipContextTest {
    private MockWebServer server;

    private final Duration applyInterval = Duration.ofMillis(300);
    private final Duration failureThreshold = Duration.ofSeconds(10);

    private final String acceptedResponse = """
            {
                "node": {"host": "%s", "port": %d },
                "epoch": {"epoch": 1}
            }
            """;

    private final String rejectedResponse = """
            {
                "code": "member",
                "message": "test"
            }
            """;

    private MembershipContext context;
    private Membership membership;

    @BeforeEach
    public void setUp() throws IOException {
        server = new MockWebServer();

        context = new MembershipContext(
                Node.builder().host("local").port(4567).build(),
                new MembershipConfig().clientWith(server.url("/").toString()),
                applyInterval,
                failureThreshold
        );
        membership = new Membership(new MemberId(context.localNode(), Epoch.first()), context.failureThreshold());
    }

    @AfterEach
    public void tearDown() throws IOException {
        server.shutdown();
    }

    @Test
    public void given_cluster_is_accessible_then_the_node_can_join() throws InterruptedException {
        prepareResponses(memberJoined(context.localNode()));

        StepVerifier.withVirtualTime(context::apply)
                .expectSubscription()
                .expectNoEvent(context.applyInterval())
                .expectNext(membership)
                .verifyComplete();

        assertThat(server.takeRequest().getPath(), is(Endpoints.join()));
    }

    @Test
    public void given_cluster_not_available_then_join_will_be_retrying() throws IOException {
        server.shutdown();
        MembershipContext c1 = spy(context);

        StepVerifier.create(c1.apply(2l))
                .expectSubscription()
                .verifyError();

        verify(c1, times(3)).join();
    }

    @Test
    public void given_cluster_recovers_then_join_will_succeed() throws InterruptedException {
        prepareResponses(memberRejected(), memberRejected(), memberJoined(context.localNode()));

        StepVerifier.create(context.apply())
                .expectSubscription()
                .expectNoEvent(context.applyInterval().multipliedBy(3))
                .expectNext(membership)
                .verifyComplete();

        assertThat(server.getRequestCount(), is(3));
    }

    private MockResponse memberJoined(Node localNode) {
        return new MockResponse()
                .setHeader("Content-Type", "application/json")
                .setBody(String.format(acceptedResponse, localNode.getHost(), localNode.getPort()));
    }

    private MockResponse memberRejected() {
        return new MockResponse()
                .setHeader("Content-Type", "application/json")
                .setResponseCode(HttpStatus.UNPROCESSABLE_ENTITY.value())
                .setBody(rejectedResponse);
    }

    private void prepareResponses(MockResponse... responses) {
        for (MockResponse response : responses) server.enqueue(response);
    }
}
