package io.odsp.subscription.replicator.replication.process;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import io.odsp.subscription.replicator.replication.offset.ReplicationWatermark;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class ReplicationSourceTest {
    private FileSystem fs;
    private final String SourceDirectory = "/foo/bar";

    @BeforeEach
    public void setUp() {
        fs = Jimfs.newFileSystem(Configuration.unix());
    }

    @AfterEach
    public void tearDown() throws IOException {
        fs.close();
    }

    @Test
    public void listRequiredBuckets() throws IOException {
        //given a certain source path
        Path sourcePath = fs.getPath(SourceDirectory);
        Files.createDirectories(sourcePath);

        //and three buckets in it
        Path b1 = sourcePath.resolve( "b1");
        Files.createDirectory(b1);
        ReplicationBucket bucket1 = new ReplicationBucket(b1, Files.getLastModifiedTime(b1));

        Path b2 = sourcePath.resolve( "b2");
        Files.createDirectory(b2);
        ReplicationBucket bucket2 = new ReplicationBucket(b2, Files.getLastModifiedTime(b2));

        Path b3 = sourcePath.resolve( "b3");
        Files.createDirectory(b3);
        ReplicationBucket bucket3 = new ReplicationBucket(b3, Files.getLastModifiedTime(b3));

        ReplicationSource source = new ReplicationSource(sourcePath);

        //when list buckets since epoch
        assertThat(source.ls(Stream.of(ReplicationWatermark.epoch())), contains(bucket1, bucket2, bucket3));

        //when list buckets since b1
        assertThat(source.ls(Stream.of(ReplicationWatermark.of("b2", Files.getLastModifiedTime(b3).toInstant(), "file"))), contains(bucket2, bucket3));

    }
}
