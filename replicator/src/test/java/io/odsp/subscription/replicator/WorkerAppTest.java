package io.odsp.subscription.replicator;

import io.odsp.subscription.commons.protocol.Endpoints;
import io.odsp.subscription.commons.protocol.membership.Epoch;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {}
)
@ActiveProfiles("docker")
@AutoConfigureWebTestClient
public class WorkerAppTest {
    @Test
    public void appTest(
            @Autowired WebTestClient webClient,
            @LocalServerPort int serverPort
    ) {

        WebTestClient.BodyContentSpec body = webClient.post().uri(Endpoints.ping(Epoch.first()))
                .exchange()
                .expectBody();

        body
                .jsonPath("$.response").isEqualTo("noMembership");
    }
}
