package io.odsp.subscription.replicator.membership;

import io.odsp.subscription.commons.protocol.membership.Node;
import org.junit.jupiter.api.Test;
import org.springframework.web.reactive.function.client.WebClient;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class MembershipConfigTest {
    private final MembershipProperties.Cluster cluster = new MembershipProperties.Cluster("http", "8.8.8.8", 12345);

    private final MembershipConfig config = new MembershipConfig();

    @Test
    public void create_local_node() throws IOException {
        Node localNode = config.localNode(cluster, 80);

        assertThat(localNode.getHost(), notNullValue());
        assertThat(localNode.getHost().toLowerCase(), not("localhost"));
        assertThat(localNode.getHost(), not("0.0.0.0"));
        assertThat(localNode.getHost(), not("127.0.0.1"));

        assertThat(localNode.getPort(), is(80));
    }

    @Test
    public void create_web_client() {
        WebClient client = config.clientWith(cluster);

        assertThat(client, notNullValue());
    }
}
