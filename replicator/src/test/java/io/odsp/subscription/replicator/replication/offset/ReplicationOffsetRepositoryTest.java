package io.odsp.subscription.replicator.replication.offset;

import io.odsp.subscription.commons.protocol.replication.Replication;
import io.odsp.subscription.commons.protocol.replication.Subscription;
import io.odsp.subscription.commons.protocol.subscription.SubscriptionId;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ActiveProfiles("docker")
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@EnableJpaAuditing
public class ReplicationOffsetRepositoryTest {
    @Autowired
    private ReplicationOffsetRepository offsetRepository;


    @Test
    public void loadOffsetOfTask() {

        SubscriptionId subscriptionId = SubscriptionId.of(100);
        String bucket = "bucket";

        //
        // Construct watermarks for three different partitions(11, 12, 21)
        ReplicationPartition p1 = ReplicationPartition.of(subscriptionId, 11);
        ReplicationPartition p2 = ReplicationPartition.of(subscriptionId, 12);
        ReplicationPartition p3 = ReplicationPartition.of(subscriptionId, 21);


        ReplicationWatermark w1 = ReplicationWatermark.of(bucket, Instant.ofEpochSecond(100_000), "a");
        ReplicationWatermark w2 = ReplicationWatermark.of(bucket, Instant.ofEpochSecond(100_000), "b");
        ReplicationWatermark w3 = ReplicationWatermark.of(bucket, Instant.ofEpochSecond(100_000), "c");
        offsetRepository.saveAll(Arrays.asList(
                new ReplicationOffset().partition(p1).watermark(w1),
                new ReplicationOffset().partition(p2).watermark(w2),
                new ReplicationOffset().partition(p3).watermark(w3)
        ));

        //
        // Save all these watermarks
        List<ReplicationOffset> offsets = offsetRepository.findAll();
        assertThat(offsets.stream().map(ReplicationOffset::partition).map(ReplicationPartition::sequence).toList(),
                contains(11, 12, 21));


        //
        //Query offset of specified task with partitions from 10 to 20
        Replication replication = mock(Replication.class);
        Subscription subscription = mock(Subscription.class);
        when(replication.subscription()).thenReturn(subscription);
        when(replication.from()).thenReturn(10);
        when(replication.to()).thenReturn(20);
        when(subscription.id()).thenReturn(subscriptionId);

        List<ReplicationOffset> offsetsOfTask = offsetRepository.offsetOf(replication);
        assertThat(offsetsOfTask.stream().map(ReplicationOffset::watermark).map(ReplicationWatermark::file).toList(), contains("a", "b"));

        //
        //set given offset
        ReplicationWatermark w11 = w1.bucket("b1").time(w1.time().plusSeconds(1)).file("z");
        offsetRepository.set(p1, w11);
        assertThat(offsetRepository.findByPartition(p1), equalTo(Optional.of(ReplicationOffset.of(p1, w11))));
    }
}
