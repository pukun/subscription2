package io.odsp.subscription.replicator.replication.process;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileTime;
import java.time.Instant;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ReplicationDestinationTest {
    private final FileSystem fs = Jimfs.newFileSystem(Configuration.unix());

    private final Path destinationPath = fs.getPath("pusher_test");
    private final String bucketName = "bucket";
    private final String fileName = "1.pm";
    private final ReplicationFile file = new ReplicationFile(bucketName, fs.getPath(bucketName, fileName), FileTime.from(Instant.now()));

    private final Charset encoding = Charset.forName("GB2312");

    @BeforeEach
    public void setUp() throws IOException {
        Files.createDirectory(destinationPath);
    }

    @Test
    public void givenFileExistedItCanBePushedAgain() throws IOException {
        ReplicationDestination destination = new ReplicationDestination(destinationPath);

        String content = "测试test中文";

        destination.dump(file, content, encoding);
        // push it for the second time
        destination.dump(file, content, encoding);

        assertThat(Files.readString(destination.pathOfFile(file), encoding), is(content));

    }
}
