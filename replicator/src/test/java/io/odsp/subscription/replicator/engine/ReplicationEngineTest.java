package io.odsp.subscription.replicator.engine;

import io.odsp.subscription.commons.protocol.membership.Epoch;
import io.odsp.subscription.commons.protocol.membership.MemberId;
import io.odsp.subscription.commons.protocol.membership.Node;
import io.odsp.subscription.replicator.membership.Membership;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class ReplicationEngineTest {
    private ReplicationEngineContext context;
    private final Node node = Node.builder().host("local").port(1000).build();
    private final MemberId memberId = new MemberId(node, Epoch.first());

    private Membership membership = mock(Membership.class);
    private Sinks.One<Void> loss;

    private ReplicationEngine engine;

    @BeforeEach
    public void setUp() {
        context = mock(ReplicationEngineContext.class);

        given(membership.memberId()).willReturn(memberId);
        loss = Sinks.one();
        given(membership.watch()).willReturn(loss.asMono());
    }

    @Nested
    @DisplayName("membership test")
    class MembershipTest {

        @Test
        @DisplayName("engine will be applying for membership and watching it once applied")
        void engine_will_be_applying_membership_and_watching_it_since_start() {
            Sinks.One<Membership> application = Sinks.one();
            given(context.applyForMembership()).willReturn(application.asMono());

            //initially, engine is not in the cluster
            engine = ReplicationEngine.start(context);
            assertThat(engine.isInCluster(memberId.epoch()), is(false));

            //when applied, engine is involved in the cluster
            application.tryEmitValue(membership);
            verify(membership).watch();
            engine.memberId().block();
            assertThat(engine.isInCluster(memberId.epoch()), is(true));

            //assign a new mono which won't yield membership to prevent from engine joining the cluster for the following test
            given(context.applyForMembership()).willReturn((Sinks.<Membership>one()).asMono());
            //when membership lost, engine will attempt to apply again
            loss.tryEmitValue((Void) null);
            assertThat(engine.memberId().block().isEmpty(), is(true));
            verify(context, times(2)).applyForMembership();
        }
    }
}
