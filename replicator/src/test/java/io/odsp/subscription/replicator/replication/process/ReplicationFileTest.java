package io.odsp.subscription.replicator.replication.process;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ReplicationFileTest {

    private final FileSystem fs = Jimfs.newFileSystem(Configuration.unix());
    private final Charset encoding = Charset.forName("GB2312");
    private final String name = "1.pm";

    @DisplayName("content of file should be read correctly")
    @Test
    public void readFile() throws IOException {
        String content = "测试test中文";

        // make a file with given name and content
        Path path = fs.getPath(name);
        Files.writeString(path, content, encoding);

        // when we load it
        String read = new ReplicationFile("bucket", path, Files.getLastModifiedTime(path)).read(encoding);

        // then the contentType is as expected
        assertThat(read, is(content));
    }
}
