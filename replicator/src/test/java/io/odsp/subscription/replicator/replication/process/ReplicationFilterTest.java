package io.odsp.subscription.replicator.replication.process;

import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;
import io.odsp.subscription.replicator.replication.offset.ReplicationWatermark;
import org.junit.jupiter.api.Test;

import java.nio.file.FileSystem;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReplicationFilterTest {

    private final FileSystem fs = Jimfs.newFileSystem(Configuration.unix());

    private final PathMatcher matcher = fs.getPathMatcher("glob:*.{pm, perm}");

    private final ReplicationPartitioner partitioner = mock(ReplicationPartitioner.class);

    private final String bucket = "bucket";
    private final Instant time = Instant.ofEpochSecond(100_000);
    private final String name = "test.pm";
    private final Map<Integer, ReplicationWatermark> watermarks = Map.of(1, ReplicationWatermark.of(bucket, time, name));

    private final ReplicationFilter filter = new ReplicationFilter(matcher, partitioner, watermarks);

    @Test
    public void shouldFilterReplicationFiles() {
        ReplicationFile file = mock(ReplicationFile.class);

        //file with name not matched is not filtered
        when(file.path()).thenReturn(Path.of("test.dat"));
        assertThat(filter.test(file), is(false));

        //file with qualified name but not in the required partition is not filtered either
        when(file.path()).thenReturn(Path.of("test.pm"));
        when(partitioner.partition(file)).thenReturn(2);
        assertThat(filter.test(file), is(false));

        //file could be filtered out because of modified time
        when(partitioner.partition(file)).thenReturn(1);
        when(file.watermark()).thenReturn(ReplicationWatermark.of(bucket, time, "test.pm"));
        assertThat(filter.test(file), is(false));

        //only most recent file can be filtered
        when(file.watermark()).thenReturn(ReplicationWatermark.of(bucket, time.plusSeconds(1), "test.pm"));
        assertThat(filter.test(file), is(true));
    }
}
