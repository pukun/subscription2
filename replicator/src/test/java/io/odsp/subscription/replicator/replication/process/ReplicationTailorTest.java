package io.odsp.subscription.replicator.replication.process;

import io.odsp.subscription.commons.content.Format;
import io.odsp.subscription.commons.content.JSONTailor;
import io.odsp.subscription.commons.content.TailorExpression;
import io.odsp.subscription.commons.content.XMLTailor;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ReplicationTailorTest {
    private final String json = """
              {
                  "firstName": "John",
                  "lastName": "Doe",
                  "age": 42,
                  "address":
                  {
                     "streetAddress": "400 Some Street",
                     "city": "Beverly Hills",
                     "state": "CA",
                     "zipcode": 90210
                  },
                  "phoneNumbers":
                  [
                     {
                        "type": "home",
                        "number": "310 555-1234"
                     },
                     {
                        "type": "fax",
                        "number": "310 555-4567"
                     }
                  ]
              }
            """;


    private final TailorExpression expression = TailorExpression.of("$.phoneNumbers[?(@.type == 'home')].number");

    private final JSONTailor jsonTailor = new JSONTailor();
    private final XMLTailor xmlTailor = new XMLTailor();
    private final ReplicationTailor tailor = new ReplicationTailor(jsonTailor, xmlTailor);

    @Test
    public void givenCorrectSpecAndContentThenTailorCouldSucceed() {
        String tailored = (tailor.tailor(json, Format.JSON, expression));
        assertThat(jsonTailor.select(tailored, "$..number").size(), is(1));
    }

    @Test
    public void givenTailorAndContentTypeDoesNotMatchThenTailorFail() {
        // tailor is of json type while contentType type is xml
        assertThrows(Throwable.class, () -> tailor.tailor(json, Format.XML, expression));
    }

}
