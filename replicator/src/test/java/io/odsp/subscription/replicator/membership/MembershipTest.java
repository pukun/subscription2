package io.odsp.subscription.replicator.membership;

import io.odsp.subscription.commons.protocol.membership.MemberId;
import org.junit.jupiter.api.Test;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.Instant;

import static org.mockito.Mockito.*;

public class MembershipTest {
    @Test
    public void membership_can_be_watched() {
        MemberId memberId = mock(MemberId.class);
        Duration failureThreshold = Duration.ofMillis(10);

        Instant now = Instant.now();
        Instant pingMoment = now.plus(failureThreshold);

        Membership membership = spy(new Membership(memberId, failureThreshold, pingMoment));

        StepVerifier.create(membership.watch(now))
                .expectSubscription()
                .expectNoEvent(failureThreshold)
                .expectNoEvent(failureThreshold)
                .verifyComplete();

        verify(membership, times(3)).isExpired(any(Instant.class));
    }
}
